package de.unibonn.creedo.studies;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import de.unibonn.creedo.common.ConnectionFactory;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;

public class EvaluationSchemeDAO {
public static final EvaluationSchemeDAO INSTANCE = new EvaluationSchemeDAO();
	
	private final SqlSessionFactory sqlSessionFactory;

	private EvaluationSchemeDAO() {
		this.sqlSessionFactory = ConnectionFactory.get().getSession();
	}
	
	public List<Integer> getAllIds() {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			
			EvaluationSchemeMapper mapper = session.getMapper(EvaluationSchemeMapper.class);
			return mapper.getAllIds();
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	
	public List<String> getInstructionsOfScheme(int evaluationSchemeId) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			EvaluationSchemeMapper mapper = session.getMapper(EvaluationSchemeMapper.class);
			return mapper.getInstructionsOfId(evaluationSchemeId);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
	
	public List<RatingMetric> getMetricsInScheme(int evaluationSchemeId) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			List<RatingMetric> results = new ArrayList<>();
			EvaluationSchemeMapper mapper = session.getMapper(EvaluationSchemeMapper.class);
			String jsonString = mapper.getMetricsInScheme(evaluationSchemeId);			
			JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
			JsonArray jsonArray = jsonObject.getAsJsonArray("metrics");
			for (JsonElement element : jsonArray) {
				results.add(RatingMetric.valueOf(element.getAsString()));
			}			
			return results;
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
