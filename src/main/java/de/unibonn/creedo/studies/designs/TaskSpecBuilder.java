package de.unibonn.creedo.studies.designs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ServerPaths;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.IdentifierListOfRepositoryParameterFactory;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.webapp.dbaccess.DataTableDAO;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class TaskSpecBuilder implements ParameterContainer {

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final RangeEnumerableParameter<Integer> inputValueParameter;

	private final SubCollectionParameter<String, List<String>> instructionParameter;

	private final Parameter<Integer> resultDefParameter;

	private final RangeEnumerableParameter<String> evaluationSchemeParameter;

	private final DefaultParameterContainer defaultParameterContainer;

	// private final int NUM_RESULTS_FOR_EVALUATION = 3;

	public TaskSpecBuilder() {
		nameParameter = new DefaultParameter<String>("Name",
				"The name of this task specification.", String.class, "",
				StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		descriptionParameter = new DefaultParameter<String>("Description",
				"A descripton that explains the task specification.",
				String.class, "", StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		instructionParameter = IdentifierListOfRepositoryParameterFactory
				.getIdentifierListOfRepositoryParameter(
						"Instructions",
						"Select instruction pages that introduce the trial task to participants. Instruction will be presented in the same order as in this list.",
						ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
						Repositories
								.getIdIsFilenameWithExtensionPredicate(Repositories.HTML_FILE_EXTENSIONS));

		resultDefParameter = new DefaultParameter<Integer>(
				"Result Definition",
				"Number of patterns that a participant needs to discovery in order to successfully submit her result.",
				Integer.class, 0, StringParser.INTEGER_PARSER,
				new ValueValidator<Integer>() {
					@Override
					public boolean valid(Integer value) {
						return value >= 0;
					}
				}, "Number of patterns in result.");

		inputValueParameter = new DefaultRangeEnumerableParameter<Integer>(
				"Input Values", "Select a data talbe of this task. ",
				Integer.class, new RangeComputer<Integer>() {
					@Override
					public List<Integer> computeRange() {
						return DataTableDAO.INSTANCE.getDatatableIds();
					}
				});

		evaluationSchemeParameter = new IdentifierInRepositoryParameter<EvaluationScheme>(
				"Evaluation Scheme",
				"Select an evaluation scheme for this task. An evaluation scheme defines the corresponding evaluation task.",
				ApplicationRepositories.STUDY_EVALUATION_SCHEME_REPOSITORY);

		defaultParameterContainer = new DefaultParameterContainer(
				"System Specification");

		defaultParameterContainer.addAllParameters(Arrays.asList(
				(Parameter<?>) nameParameter, descriptionParameter,
				instructionParameter, resultDefParameter, inputValueParameter,
				evaluationSchemeParameter));

	}

	public TaskSpecification build() {

		List<String> instructionPageHtmls = new ArrayList<>();
		List<String> fileNames = instructionParameter.getCurrentValue();
		for (String fileName : fileNames) {
			instructionPageHtmls.add(ServerPaths
					.getContentPageContent(fileName));
		}
		new ApplicationRepositories();
		return new TaskSpecification(nameParameter.getCurrentValue(),
				descriptionParameter.getCurrentValue(), instructionPageHtmls,
				resultDefParameter.getCurrentValue(),
				inputValueParameter.getCurrentValue(),
				ApplicationRepositories.STUDY_EVALUATION_SCHEME_REPOSITORY.get(
						evaluationSchemeParameter.getCurrentValue())
						.getContent());
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return defaultParameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return defaultParameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return defaultParameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		defaultParameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		defaultParameterContainer.unloadMapValuesToParameters(crossOutMap);
	}

}
