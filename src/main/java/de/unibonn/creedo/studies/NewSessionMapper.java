package de.unibonn.creedo.studies;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import de.unibonn.creedo.webapp.studies.ResultDataContainer;

/**
 * @author bjacobs, bkang
 */

public interface NewSessionMapper {

	public void saveSessionInfo(@Param("study_name") String study_name,
			@Param("user_id") int userId, @Param("task_name") String taskName,
			@Param("system_name") String systemName,
			@Param("end_time") Date date);

	public List<String> getTaskNamesPerformedByUserInStudy(
			@Param("user_id") int userId, @Param("study_name") String studyName);

	public int getLastInsertId();

	public void saveResult(@Param("session_id") int sessionId,
			@Param("result_builder_content") String resultBuilderContent,
			@Param("result_seconds_until_saved") int secondsUntilSaved);
	
	public Integer getIdOfLastSessionByUser(@Param("study_name") String studyName,
			@Param("user_id") int userId);

	public List<Integer> getPerformedSeesionIdsOfTaskInStudy(
			@Param("task_name") String taskName, @Param("study_name") String studyName);

	public Integer getNumberOfPerformedSessionsWithTaskAndAnalyticsDB(
			@Param("study_name") String studyName, @Param("task_name") String taskName,
			@Param("system_name") String systemName);
	
	public List<ResultDataContainer> getResultsInSavedSession(@Param("session_id") int sessionId);
}
