package de.unibonn.creedo.clienttools.database;

import de.unibonn.creedo.common.BCrypt;
import de.unibonn.creedo.webapp.dbaccess.DataTableDAO;
import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: bjacobs
 * Date: 04.03.15
 * Time: 13:49
 */

public class InitDatabaseUtil {
    /**
     * Creates a connection to the database
     * - directory
     * - host
     * - port
     * - database
     * - user
     * - password
     * - dropexisting
     */
    public static void main(String[] args) {
	if (args.length < 7) {
	    System.out.println("Usage: [dir to sql files] [hostname] [port] [database name] [username] [password] [adminemail] [adminpassword] [dropexisting true/false]");
	    System.exit(-1);
	}

	String directory = new File(args[0]).getAbsolutePath();
	String hostname = args[1];
	String port = args[2];
	String databaseName = args[3];
	String username = args[4];
	String password = args[5];
	String adminEmail = args[6];
	String adminPassword = args[7];
	Boolean dropExisting = Boolean.valueOf(args[8]);

	try {
	    initialize(directory, hostname, port, databaseName, username, password, adminEmail, adminPassword, dropExisting);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public static void initialize(String directory, String host, String port, String database, String username, String password, String adminEmail, String adminPass, Boolean dropExisting) {

	List<File> sqlCreateScriptFiles = findSqlCreateScripts(directory);

	try {
	    String hashedAdminPass = BCrypt.hashpw(adminPass, BCrypt.gensalt(12));
	    List<Reader> readers = getReaders(database, sqlCreateScriptFiles, adminEmail, hashedAdminPass, dropExisting);

	    Connection conn = getConnection(host, port, username, password);
	    ScriptRunner runner = new ScriptRunner(conn);
	    runner.setLogWriter(null);
	    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
	    PrintWriter outWriter = new PrintWriter(outStream);

	    runner.setErrorLogWriter(outWriter);
	    runner.setCharacterSetName("UTF-8");
	    for (Reader reader : readers) {
		runner.runScript(reader);
		System.out.println("Executed SQL script: " + reader.toString());

		if (outStream.size() > 0) {
		    throw new RuntimeException("Error starts with: " +
			    new String(Arrays.copyOfRange(outStream.toByteArray(), 0, 1000)));
		}
	    }
	    conn.close();


	    // Now update datatable visibility for admin
	    List<Integer> datatableIds = new DataTableDAO().getDatatableIds();

	    if (dropExisting) {
		conn = getConnection(host, port, username, password);
		runner = new ScriptRunner(conn);
		runner.setLogWriter(null);
		outStream = new ByteArrayOutputStream();
		outWriter = new PrintWriter(outStream);
		runner.setErrorLogWriter(outWriter);
		runner.setCharacterSetName("UTF-8");

		runner.runScript(new StringReader("USE " + database + ";"));
		// Add visibility to all datatables for admin
		for (Integer datatableId : datatableIds) {
		    runner.runScript(new StringReader("INSERT INTO `datatable_isvisibleto_user` VALUES (1, " + datatableId + ");"));
		}

		conn.close();
	    }

	} catch (Exception e) {
	    StringBuilder builder = new StringBuilder();
	    builder.append("Initialization failed with dir=").append(directory)
		    .append(" hostname=").append(host)
		    .append(" port=").append(port)
		    .append(" database=").append(database)
		    .append(" username=").append(username)
		    .append("\nMessage: ").append(e.getMessage());
	    String msg = builder.toString();

	    throw new RuntimeException(msg, e);
	}


    }

    private static List<Reader> getReaders(String database, List<File> sqlCreateScriptFiles, String adminEmail, String adminHashedPass, Boolean dropExisting) throws FileNotFoundException {
	List<Reader> readers = new ArrayList<>();

	if (dropExisting) {
	    readers.add(new StringReader("DROP DATABASE IF EXISTS `" + database + "`;"));
	    readers.add(new StringReader("CREATE DATABASE `" + database + "`;"));
	}
	readers.add(new StringReader("USE `" + database + "`;"));

	for (File file : sqlCreateScriptFiles) {
	    readers.add(new FileReader(file));
	}

	if (dropExisting) {
	    // Add default admin user
	    readers.add(new StringReader("INSERT INTO `user` VALUES (1, '" + adminEmail + "', '" + adminHashedPass + "', 1, 1, 1, 1, '2014-01-01 01:00:00');"));
	}

	return readers;
    }

    private static Connection getConnection(String host, String port, String username, String password) throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
	String connectionString = "jdbc:mysql://" + host + ":" + port + "/";
	Class.forName("com.mysql.jdbc.Driver").newInstance();
	return DriverManager.getConnection(connectionString, username, password);
    }

    private static List<File> findSqlCreateScripts(String databaseFilesDir) {
	File dir = new File(databaseFilesDir);

	if (!dir.isDirectory()) {
	    throw new IllegalArgumentException(dir.getAbsolutePath() + " is no directory!");
	}
	File[] files = dir.listFiles(new FileFilter() {
	    @Override
	    public boolean accept(File pathname) {
		return !pathname.isDirectory() && pathname.getName().toLowerCase().endsWith(".sql");
	    }
	});
	List<File> filesList = Arrays.asList(files);
	Collections.sort(filesList);
	return filesList;
    }

}
