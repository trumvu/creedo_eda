/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.creedo.webapp;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.CustomDashboardCreationPage;
import de.unibonn.creedo.webapp.dashboard.AnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.utils.JsonTransferParameterContainer;
import de.unibonn.creedo.webapp.utils.ParameterToJsonConverter;
import de.unibonn.realkd.common.logger.LogChannel;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.common.parameter.Parameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bjacobs
 */
@Controller()
@RequestMapping(value = "/customDash")
public class CustomDashboardCreationPageController {
	private static final LogChannel log = LogChannel.CUSTOM_DASHBOARD;

	public static final String POST_CUSTOM_DASHBOARD_PARAM_URL = "setSystemVariant.json";

	public static final String POST_CUSTOM_DASHBOARD_SET_PARAM_URL = "setParameter.json";

	public static final String GET_CUSTOM_DASHBOARD_GET_PARAM_URL = "getParameters.json";

	public static final String POST_CREATE_CUSTOM_DASHBOARD_URL = "createCustomDashboard.htm";

	public static final String POST_UPLOAD_DATABASE_URL = "uploadDatabase.htm";

	public static final String POST_CREATE_DASHBOARD_URL = "createDashboard.htm";

	@Autowired
	private HttpSession session;

	@RequestMapping(value = "/" + POST_UPLOAD_DATABASE_URL, method = RequestMethod.POST)
	public String uploadDatabase(
			@RequestParam(value = "databasename") String databaseName,
			@RequestParam(value = "databasedescription") String databaseDescription,
			@RequestParam(value = "attributesfile", required = false) MultipartFile attributesFile,
			@RequestParam(value = "datafile", required = false) MultipartFile dataFile,
			@RequestParam(value = "attributegroupsfile", required = false) MultipartFile attributeGroupsFile,
			@RequestParam(value = "datadelimiter", required = false) String dataDelimiter,
			@RequestParam(value = "missingsymbol", required = true) String missingSymbol) {

		CreedoSession creedoSession = Creedo.getCreedoSession(session);
		try {
			creedoSession.getDatabaseUploadPage().addDatabase(
					attributesFile, dataFile, attributeGroupsFile, dataDelimiter, missingSymbol,
					databaseName, databaseDescription);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/index.htm";
	}

//	@RequestMapping(value = "/" + POST_CREATE_DASHBOARD_URL, method = RequestMethod.POST)
//	public String createDashboard(
//			@RequestParam(value = "dashboardTitle") String dashboardTitle,
//			@RequestParam(value = "dashboardDescription") String dashboardDescription,
//			@RequestParam(value = "systemVariantSelect") Integer systemVariantId,
//			@RequestParam(value = "dataTableSelect") Integer dataTableId,
//			@RequestParam(value = "algorithm") Integer[] userSelectedAlgorithmIds) {
//
//		CreedoSession creedoSession = CreedoSession.getCreedoSession(session);
//		try {
//			creedoSession.getDemoDashboardCreationPage().createDashboard(dashboardTitle,
//					dashboardDescription, systemVariantId, dataTableId, userSelectedAlgorithmIds);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return "redirect:/index.htm";
//	}

	@RequestMapping(value = "/" + POST_CREATE_CUSTOM_DASHBOARD_URL, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> createCustomDashboard(
			@RequestParam(value = "attributesfile", required = false) MultipartFile attributesFile,
			@RequestParam(value = "datafile", required = false) MultipartFile dataFile,
			@RequestParam(value = "attributegroupsfile", required = false) MultipartFile attributeGroupsFile,
			@RequestParam(value = "dataTableSelect", required = false) Integer dataTableId,
			@RequestParam(value = "datadelimiter", required = false) String dataDelimiter,
			@RequestParam(value = "missingsymbol", required = true) String missingSymbol,
			@RequestParam(value = "algorithm", required = true) Integer[] userSelectedAlgorithmIds,
			@RequestParam(value = "selectedTabIndex", required = true) Integer selectedTabIndex)
			throws Exception {
		CreedoSession creedoSession = Creedo.getCreedoSession(session);

		CustomDashboardCreationPage customDashModel = creedoSession
				.getCustomDashboardCreationPage();

		try {
			AnalyticsDashboardBuilder builder = customDashModel
					.createCustomDashboardBuilder(attributesFile, dataFile,
							attributeGroupsFile, dataTableId, dataDelimiter,
							missingSymbol, userSelectedAlgorithmIds,
							selectedTabIndex);

			creedoSession.createMiningDashboard(builder);
			return new ResponseEntity<>(DashboardController.GET_DASHBOARD_URL,
					HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/" + POST_CUSTOM_DASHBOARD_PARAM_URL, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> setSystemVariant(
			@RequestParam(value = "systemVariantId", required = true) Integer systemVariantId)
			throws Exception {
		CreedoSession creedoSession = Creedo.getCreedoSession(session);

		CustomDashboardCreationPage customDashModel = creedoSession
				.getCustomDashboardCreationPage();

		log.log("System variant set to " + systemVariantId,
				LogMessageType.INTRA_COMPONENT_MESSAGE);
		try {
			customDashModel.setSystemVariant(systemVariantId);

			return new ResponseEntity<>("{}", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/" + POST_CUSTOM_DASHBOARD_SET_PARAM_URL, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> setParameter(
			@RequestParam(value = "parameterName", required = true) String parameterName,
			@RequestParam(value = "parameterValue", required = true) String parameterValue)
			throws Exception {
		CreedoSession creedoSession = Creedo.getCreedoSession(session);

		CustomDashboardCreationPage customDashModel = creedoSession
				.getCustomDashboardCreationPage();

		try {
			log.log("Trying to set parameter \"" + parameterName
					+ "\" to value \"" + parameterValue + "\"",
					LogMessageType.INTRA_COMPONENT_MESSAGE);

			customDashModel.setParameter(parameterName, parameterValue);

			return new ResponseEntity<>("{}", HttpStatus.OK);
		} catch (Exception e) {
			log.log(e.getMessage(), LogMessageType.INTRA_COMPONENT_MESSAGE);
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/" + GET_CUSTOM_DASHBOARD_GET_PARAM_URL, method = RequestMethod.GET)
	@ResponseBody
	public List<JsonTransferParameterContainer> getParameters() {
		CreedoSession creedoSession = Creedo.getCreedoSession(session);

		CustomDashboardCreationPage customDashboardCreationPage = creedoSession
				.getCustomDashboardCreationPage();

		try {
			log.log("Retrieving parameters from current mining-system builder",
					LogMessageType.INTRA_COMPONENT_MESSAGE);

			List<Parameter> parameters = customDashboardCreationPage.getParameters();

			// Convert to JSON transfer containers + return
			return ParameterToJsonConverter.convertRecursively(parameters);
		} catch (Exception e) {
			log.log(e.getMessage(), LogMessageType.INTRA_COMPONENT_MESSAGE);
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

}
