package de.unibonn.creedo.webapp;


import java.io.Serializable;

public class User implements Serializable {

    public static final User DEFAULT_USER = createDefaultUser();

    private static User createDefaultUser() {
	User user = new User();
	user.setUsername("default");
	user.setPassword("");
	user.setActive(true);
	user.setId(-1);
	return user;
    }

    private static final long serialVersionUID = -7127823971676513458L;

    private int id;
    private String username = null;
    private String password = null;
    private boolean isDeveloper;
    private boolean isActive;
    private boolean isAdministrator;
    // private int ageGroup;
    private int sex;
    private String registrationDate;
/*    private int domainExpertiseLevel;
    private int dataAnalysisExpertiseLevel;
    private int educationLevel;
    private String educationSubject;
    private String recentOccupation;*/

    public void activate() {
	isActive = true;
    }

    public int getId() {
	return id;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public boolean isDeveloper() {
	return isDeveloper;
    }

    public void setDeveloper(boolean developer) {
	isDeveloper = developer;
    }

    public boolean isActive() {
	return isActive;
    }

    public void setActive(boolean active) {
	isActive = active;
    }

    public boolean isAdministrator() {
	return isAdministrator;
    }

    public void setAdministrator(boolean administrator) {
	isAdministrator = administrator;
    }

/*    public int getAgeGroup() {
	return ageGroup;
    }

    public void setAgeGroup(int ageGroup) {
        this.ageGroup = ageGroup;
    }*/

    public int getSex() {
	return sex;
    }

    public void setSex(int sex) {
	this.sex = sex;
    }

    public String getRegistrationDate() {
	return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
	this.registrationDate = registrationDate;
    }

/*    public int getDomainExpertiseLevel() {
        return domainExpertiseLevel;
    }

    public void setDomainExpertiseLevel(int domainExpertiseLevel) {
        this.domainExpertiseLevel = domainExpertiseLevel;
    }

    public int getDataAnalysisExpertiseLevel() {
        return dataAnalysisExpertiseLevel;
    }

    public void setDataAnalysisExpertiseLevel(int dataAnalysisExpertiseLevel) {
        this.dataAnalysisExpertiseLevel = dataAnalysisExpertiseLevel;
    }

    public int getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(int educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getEducationSubject() {
        return educationSubject;
    }

    public void setEducationSubject(String educationSubject) {
        this.educationSubject = educationSubject;
    }

    public String getRecentOccupation() {
        return recentOccupation;
    }

    public void setRecentOccupation(String recentOccupation) {
        this.recentOccupation = recentOccupation;
    }*/
}
