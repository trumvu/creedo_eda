package de.unibonn.creedo.webapp.viewmodels;

import static de.unibonn.realkd.common.logger.LogChannel.UI;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;

public class MetaDataTableModel {

	private PropositionalLogic propLogic;

	public MetaDataTableModel(PropositionalLogic propLogic) {
		UI.log("Creating new metadata view model",
				LogMessageType.INTER_COMPONENT_MESSAGE);
		this.propLogic = propLogic;
	}

	public List<Attribute> getNonIDAttributes() {
		return propLogic.getDatatable().getAttributes();
	}

	public Attribute getNonIDAttribute(int attributeIndex) {
		return getNonIDAttributes().get(attributeIndex);
	}

	public String getAttributeType(Attribute attribute) {
		String result;
		if (attribute instanceof CategoricalAttribute) {
			result = "Categorical";
		} else if (attribute instanceof MetricAttribute) {
			result = "Metric";
		} else if (attribute instanceof OrdinalAttribute) {
			result = "Ordinal";
		} else {
			throw new IllegalArgumentException(
					"Must be called with non id attribute");
		}

		return result;
	}

	public String getConstraintDescriptionAbout(Attribute attribute) {
		StringBuffer result = new StringBuffer();
		for (Proposition prop : propLogic.getPropositionsAbout(attribute)) {
			result.append(prop.getConstraint().getDescription() + ", ");
		}

		if (result.length() > 0) {
			result.delete(result.length() - 2, result.length());
		}

		return result.toString();
	}

	public List<AttributeMetaDataContainer> getEntries() {
		List<AttributeMetaDataContainer> result = new ArrayList<>();
		for (Attribute attr : getNonIDAttributes()) {
			result.add(new AttributeMetaDataContainer(attr));
		}
		return result;
	}

	public class AttributeMetaDataContainer {
		public String getName() {
			return name;
		}

		public String getType() {
			return type;
		}

		public String getDescription() {
			return description;
		}

		private final String name;
		private final String type;
		private final String description;

		public AttributeMetaDataContainer(Attribute attr) {
			this.name = attr.getName();
			this.type = getAttributeType(attr);
			this.description = getConstraintDescriptionAbout(attr);
		}
	}

}
