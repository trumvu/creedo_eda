package de.unibonn.creedo.webapp.viewmodels;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.common.Inspectable;
import de.unibonn.creedo.common.InspectableInspector;

public class DeveloperViewModel implements InspectableInspector {

	private List<Inspectable> inspectables = new ArrayList<>();

	@Override
	public void tellInspectable(Inspectable inspectable) {
		this.inspectables.add(inspectable);
	}

	public String getHtml() {
		StringBuilder sb = new StringBuilder();
		for (Inspectable inspectable : inspectables) {
			sb.append("<b><big>" + (inspectables.indexOf(inspectable) + 1)
					+ ". " + inspectable.getName() + "</big></b><br/>");
			for (String s : inspectable.getDescriptions()) {
				sb.append("<b>" + s + "</b><br>");
			}
			sb.append(getTableHtml(
					inspectable.getInspectableState().getTable(), inspectable
							.getInspectableState().getHeader()));
			List<Inspectable> subInspectables = getAllSubInspectables(inspectable);
			for (Inspectable subInspectable : subInspectables) {
				sb.append("<b>" + (inspectables.indexOf(inspectable) + 1) + "."
						+ (subInspectables.indexOf(subInspectable) + 1) + " "
						+ subInspectable.getName() + "</b>");
				sb.append(getTableHtml(subInspectable.getInspectableState()
						.getTable(), subInspectable.getInspectableState()
						.getHeader()));
			}
		}
		return sb.toString();

	}

	private String getTableHtml(List<List<String>> table, List<String> header) {
		StringBuilder sb = new StringBuilder();
		sb.append("<div style=\"height: 300px; overflow: auto;\">");
		sb.append("<table border=\"1\" style=\"width: 1000px\"><tr>");
		for (String s : header) {
			sb.append("<td>" + s + "</td>");
		}
		sb.append("</tr><tr>");
		for (List<String> row : table) {
			sb.append("<tr>");
			for (String s : row) {
				sb.append("<td>" + s + "</td>");
			}
			sb.append("</tr>");
		}
		sb.append("</table>");
		sb.append("</div>");
		return sb.toString();
	}

	private List<Inspectable> getAllSubInspectables(Inspectable inspectable) {
//		if (inspectable.getSubInspectables() == null
//				|| inspectable.getSubInspectables().size() == 0) {
//			return new ArrayList<>();
//		}
		List<Inspectable> subInspectables = new ArrayList<Inspectable>();
		for (Inspectable ins : inspectable.getSubInspectables()) {
			subInspectables.add(ins);
			subInspectables.addAll(getAllSubInspectables(ins));
		}
		return subInspectables;
	}
}
