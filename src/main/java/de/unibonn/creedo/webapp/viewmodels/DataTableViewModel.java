package de.unibonn.creedo.webapp.viewmodels;

import static de.unibonn.realkd.common.logger.LogChannel.UI;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.utils.ServerVisualizationTools;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricalAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;
import de.unibonn.realkd.data.table.attributegroups.AttributeGroup;
import de.unibonn.realkd.visualization.attribute.AttributeView;
import de.unibonn.realkd.visualization.attribute.CategoricalAttributePieView;
import de.unibonn.realkd.visualization.attribute.NumericAttributeBoxPlotView;
import de.unibonn.realkd.visualization.attribute.NumericAttributeHistogramView;

/**
 * Class provides model for the visual datatable that is rendered in web app. It
 * wraps an abstract core data table. All decisions regarding what data is
 * displayed is implemented here.
 * 
 * @author Mario Boley
 * 
 */
public class DataTableViewModel {

	private static final String DISPLAY_SYMBOL_FOR_MISSING_VALUE = "?";

	private static final String NAME_COLUMN_TITLE = "Name";

	/**
	 * static list of attribute illustrators that provide illustrations for
	 * attribute tool-tips; each is checked against every attributes for
	 * applicability
	 */
	private final static List<AttributeView> ATTRIBUTE_ILLUSTRATORS;
	static {
		ATTRIBUTE_ILLUSTRATORS = new ArrayList<>();
		ATTRIBUTE_ILLUSTRATORS.add(new CategoricalAttributePieView());
		ATTRIBUTE_ILLUSTRATORS.add(new NumericAttributeBoxPlotView());
		ATTRIBUTE_ILLUSTRATORS.add(new NumericAttributeHistogramView());
	}

	private enum AttributeStatisticsMapper {

		GENERAL_MAPPER {
			@Override
			public boolean isApplicable(DataTable dataTable, Attribute attribute) {
				return true;
			}

			@Override
			public void addStatisticsHtml(DataTable dataTable,
					Attribute attribute, StringBuilder partialHtml) {
				// Collection<AttributeGroup> groups = attribute
				// .getAttributeGroups();
				Collection<AttributeGroup> groups = dataTable
						.getAttributeGroupsStore().getAttributeGroupsOf(
								attribute);
				int numberOfMissings = attribute.getMissingPositions().size();

				if (!groups.isEmpty() || numberOfMissings > 0) {
					partialHtml.append("<br />");
				}
				if (!groups.isEmpty()) {
					partialHtml.append("groups: ");
					for (AttributeGroup group : groups) {
						partialHtml.append("<b>"+group.getName()+"</b>" + ", ");
					}
					partialHtml.append("<br />");
				}
				if (numberOfMissings > 0) {
					partialHtml
							.append(numberOfMissings + "/"
									+ (attribute.getMaxIndex() + 1)
									+ " values missing");
					partialHtml.append("<br />");
				}

				partialHtml.append("<br />");
			}
		},

		CATEGORIC_MAPPER {
			@Override
			public boolean isApplicable(DataTable dataTable, Attribute attribute) {
				return (attribute instanceof CategoricalAttribute);
			}

			@Override
			public void addStatisticsHtml(DataTable dataTable,
					Attribute attribute, StringBuilder partialHtml) {
				CategoricalAttribute categoricAttribute = (CategoricalAttribute) attribute;
				for (int i = 0; i < categoricAttribute.getCategories().size(); i++) {
					partialHtml.append(categoricAttribute.getCategories()
							.get(i)
							+ ": "
							+ String.format(Locale.ENGLISH, "%.4f",
									categoricAttribute.getCategoryFrequencies()
											.get(i)) + "<br />");
				}

			}

		},

		ORDINAL_MAPPER {
			@Override
			public boolean isApplicable(DataTable dataTable, Attribute attribute) {
				return attribute instanceof OrdinalAttribute &&
						!(attribute instanceof MetricAttribute);
			}

			@Override
			public void addStatisticsHtml(DataTable dataTable, Attribute attribute, StringBuilder partialHtml) {
				OrdinalAttribute ordinalAttribute = (OrdinalAttribute) attribute;

				partialHtml.append("Maximum: ").append(
						ordinalAttribute.getMax()).append("<br />");
				partialHtml.append("Lwr. Qrt.: ").append(
						ordinalAttribute.getLowerQuartile()).append("<br />");
				partialHtml.append("Median: ").append(
						ordinalAttribute.getMedian()).append("<br />");
				partialHtml.append("Upr. Qrt.: ").append(
						ordinalAttribute.getUpperQuartile()).append(
						"<br />");
				partialHtml.append("Minimum: ").append(
						ordinalAttribute.getMin()).append("<br />");
			}
		},

		NUMERIC_MAPPER {
			public boolean isApplicable(DataTable dataTable, Attribute attribute) {
				return (attribute instanceof MetricAttribute);
			}

			@Override
			public void addStatisticsHtml(DataTable dataTable,
					Attribute attribute, StringBuilder partialHtml) {
				MetricAttribute metricAttribute = (MetricAttribute) attribute;
				partialHtml.append("Maximum: "
						+ String.format(Locale.ENGLISH, "%.4f",
						metricAttribute.getMax()) + "<br />");
				partialHtml
						.append("Lwr. Qrt.: "
								+ String.format(Locale.ENGLISH, "%.4f",
								metricAttribute.getLowerQuartile())
								+ "<br />");
				partialHtml.append("Median: "
						+ String.format(Locale.ENGLISH, "%.4f",
						metricAttribute.getMedian()) + "<br />");
				partialHtml
						.append("Upr. Qrt.: "
								+ String.format(Locale.ENGLISH, "%.4f",
								metricAttribute.getUpperQuartile())
								+ "<br />");
				partialHtml.append("Mean: "
						+ String.format(Locale.ENGLISH, "%.4f",
								metricAttribute.getMean()) + "<br />");
				partialHtml.append("Standard deviation: "
						+ String.format(Locale.ENGLISH, "%.4f",
								metricAttribute.getStandardDeviation())
						+ "<br />");
				partialHtml.append("Variance: "
						+ String.format(Locale.ENGLISH, "%.4f",
								metricAttribute.getVariance()) + "<br />");
				partialHtml.append("Skew: "
						+ String.format(Locale.ENGLISH, "%.4f",
								metricAttribute.getSkew()));
			}

		};

		public abstract boolean isApplicable(DataTable dataTable,
				Attribute attribute);

		public abstract void addStatisticsHtml(DataTable dataTable,
				Attribute attribute, StringBuilder partialHtml);
	}

	private final DataTable dataTable;

	private final HttpSession httpSession;

	private List<List<String>> dataTableContent;

	private List<String> attributeTooltips;

	public DataTableViewModel(DataTable dataTable, HttpSession session) {
		this.dataTable = dataTable;
		this.httpSession = session;
		initDataTableContent();
		initAttributeTooltips();
	}

	private void initDataTableContent() {
		dataTableContent = new ArrayList<>(dataTable.getSize());
		for (int i = 0; i < dataTable.getSize(); i++) {
			List<String> row = new ArrayList<>(dataTable.getAttributes().size());
			row.add(dataTable.getObjectName(i));
			for (Attribute attribute : dataTable.getAttributes()) {
				if (attribute.isValueMissing(i)) {
					row.add(DISPLAY_SYMBOL_FOR_MISSING_VALUE);
				} else {
					row.add(attribute.getValue(i).toString());
				}
			}
			dataTableContent.add(row);
		}
	}

	private void initAttributeTooltips() {
		UI.log("Generating attribute tool-tips",
				LogMessageType.INTER_COMPONENT_MESSAGE);
		List<String> tooltips = new ArrayList<>();
		List<Attribute> attributes = dataTable.getAttributes();
		for (int i = 0; i < attributes.size(); i++) {
			StringBuilder tooltip = new StringBuilder();
			tooltip.append(attributes.get(i).getDescription());
			tooltip.append("<hr class='no-margin' />");

			for (AttributeStatisticsMapper mapper : AttributeStatisticsMapper
					.values()) {
				if (mapper.isApplicable(dataTable,attributes.get(i))) {
					mapper.addStatisticsHtml(dataTable,attributes.get(i), tooltip);
				}
			}

			List<String> attrUrlList = new ArrayList<>();

			for (AttributeView attributeView : ATTRIBUTE_ILLUSTRATORS) {
				if (attributeView.isApplicable(dataTable.getAttribute(i))) {
					attrUrlList.add(ServerVisualizationTools.serveChartAsPNG(
							httpSession,
							attributeView.draw(dataTable.getAttribute(i)),
							attributeView.getDefaultWidth(),
							attributeView.getDefaultHeight()));
				}
			}
			tooltip.append("<div class='column-visualization'>");
			for (String imgUrl : attrUrlList) {
				tooltip.append("<br/>");
				tooltip.append("<img src='" + imgUrl + "' alt='url preview' />");
			}
			tooltip.append("</div>");

			tooltips.add(tooltip.toString());
		}
		this.attributeTooltips = tooltips;
	}

	public List<String> getAttributeColumnTitles() {
		List<String> columnHeaders = new ArrayList<String>();
		columnHeaders = new ArrayList<String>(
				dataTable.getNumberOfAttributes() + 1);
		for (Attribute attribute : dataTable.getAttributes()) {
			columnHeaders.add(attribute.getName());
		}
		return columnHeaders;
	}

	public String getNameColumnTitle() {
		return NAME_COLUMN_TITLE;
	}

	public List<List<String>> getDataTableContent() {
		return dataTableContent;
	}

	public List<String> getColumnToolTipHtml() {
		return this.attributeTooltips;
	}

}
