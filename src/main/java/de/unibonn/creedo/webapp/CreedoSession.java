package de.unibonn.creedo.webapp;

import static de.unibonn.realkd.common.logger.LogChannel.UI;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ui.CustomDashboardCreationPage;
import de.unibonn.creedo.ui.DatabaseUploadPage;
import de.unibonn.creedo.ui.core.DefaultPageFrame;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.PageFrame;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister;
import de.unibonn.creedo.webapp.dashboard.AnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.realkd.common.logger.LogMessageType;

/**
 * Holds the state of the application session. Requesting the current
 * application session is the entry point in to the application logic for all
 * application requests (different from root requests for application setup).
 * 
 * There is at most one CreedoSession bound to the HttpSession trough which a
 * request is issued.
 * 
 * @author mboley
 * 
 */
public class CreedoSession {

	private final int loadLoginPageActionId;

	public static final String UISESSION_HTTPSESSION_PROPERTY_NAME = "uisession";

	private final User user;

	private final HttpSession httpSession;

	private final UiRegister uiRegister;

	private DashboardModel dashboard = null;

	private DefaultPageFrame helpFrame;

	private final DatabaseUploadPage databaseUploadPage;

	public static Object retrieveObjectWithCreedoSessionKey(HttpSession httpSession) {
		return httpSession
				.getAttribute(CreedoSession.UISESSION_HTTPSESSION_PROPERTY_NAME);
	}

	/**
	 * Creates a new creedo session for user and binds it to httpSession
	 * (replacing old session).
	 * 
	 * NOTE: old session should also be invalidated and resources freed up
	 * 
	 */
	public static void createNewCreedoSession(User user, HttpSession httpSession) {
		CreedoSession uiSession = new CreedoSession(user, httpSession);
		httpSession
				.setAttribute(UISESSION_HTTPSESSION_PROPERTY_NAME, uiSession);
	}

	private CreedoSession(User user, HttpSession httpSession) {
		this.httpSession = httpSession;
		this.user = user;
		this.uiRegister = new UiRegister(httpSession);
		this.loadLoginPageActionId = uiRegister.getNextId();

		customDashboardCreationPage = new CustomDashboardCreationPage();
		databaseUploadPage = new DatabaseUploadPage();

		this.mainFrame = (DefaultPageFrame) ApplicationRepositories.FRAME_REPOSITORY
				.get("mainframe").getContent().build(this);

		this.helpFrame = (DefaultPageFrame) ApplicationRepositories.FRAME_REPOSITORY
				.get("helpframe").getContent().build(this);

	}

	public int getLoadLoginPageActionId() {
		return loadLoginPageActionId;
	}

	public User getUser() {
		return user;
	}

	private DefaultPageFrame mainFrame = null;

	private CustomDashboardCreationPage customDashboardCreationPage = null;

	public PageFrame getMainFrame() {
		return mainFrame;
	}

	public Frame getHelpFrame() {
		return helpFrame;
	}

	/**
	 * Creates a new dashboard inside this Creedo session that based on the
	 * provided configuration. Tears down current root-level dashboard if one
	 * exists.
	 * 
	 */
	public void createMiningDashboard(AnalyticsDashboardBuilder builder) {
		tearDownDashboard();
		// DashboardModel model = builder.build(httpSession);
		DashboardModel model = uiRegister.createFrameFromBuilder(builder);
		// DashboardModel test frameFactory.createFrameFromBuilder(builder);
		UI.log("Create new analysis session based on: " + model,
				LogMessageType.GLOBAL_STATE_CHANGE);

		dashboard = model;
	}

	/**
	 * Frees the resources claimed by current root-level dashboard (if there is
	 * one).
	 * 
	 */
	public void tearDownDashboard() {
		UI.log("Trying to tear down current analysis session",
				LogMessageType.GLOBAL_STATE_CHANGE);
		if (dashboard != null) {
			dashboard.tearDown();
			dashboard = null;
		}

		// stopTimeoutHandler();
	}

	// private void stopTimeoutHandler() {
	// Object toHandlerObj = httpSession
	// .getAttribute(WebConstants.TIMEOUT_HANDLER_KEY);
	// ((TimeoutHandler) toHandlerObj).stop();
	// }

	public DashboardModel getDashboard() {
		return dashboard;
	}

	/**
	 * Recursively searches dashboard with specified id starting in the root
	 * dashboard registered in this.
	 * 
	 */
	public DashboardModel getDashboard(int id) {
		if (dashboard == null) {
			return null;
		}
		return dashboard.getDashboard(id);
	}

	public CustomDashboardCreationPage getCustomDashboardCreationPage() {
		return customDashboardCreationPage;
	}

	public DatabaseUploadPage getDatabaseUploadPage() {
		return databaseUploadPage;
	}

	public UiRegister getUiRegister() {
		return this.uiRegister;
	}

	public UiComponent getUiComponent(int componentId) {
		return uiRegister.retrieveUiComponent(componentId);
	}

	public HttpSession getHttpSession() {
		return httpSession;
	}
}
