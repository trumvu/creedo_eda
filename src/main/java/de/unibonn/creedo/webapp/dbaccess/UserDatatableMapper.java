package de.unibonn.creedo.webapp.dbaccess;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * @author bjacobs
 */

public interface UserDatatableMapper {
	List<Integer> getDatatableIdsForUser(@Param("userid") int id);

	void addDatatableIdForUser(
		@Param("userid") int id,
		@Param("datatableid") int datatableId);
}
