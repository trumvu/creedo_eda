package de.unibonn.creedo.webapp.dbaccess;

/**
 * @author bjacobs
 */

public class DashboardLinkConfig {
	private int id;
	private String title;
	private String description;
	private String imgUrl;
	private String imgCredits;
	private int analyticsDashboardId;
	private int dataTableId;

	public DashboardLinkConfig() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getImgCredits() {
		return imgCredits;
	}

	public void setImgCredits(String imgCredits) {
		this.imgCredits = imgCredits;
	}

	public int getAnalyticsDashboardId() {
		return analyticsDashboardId;
	}

	public void setAnalyticsDashboardId(int analyticsDashboardId) {
		this.analyticsDashboardId = analyticsDashboardId;
	}

	public void setDataTableId(int dataTableId) {
		this.dataTableId = dataTableId;
	}

	public int getDataTableId() {
		return dataTableId;
	}
}
