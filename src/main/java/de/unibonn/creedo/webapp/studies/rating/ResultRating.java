package de.unibonn.creedo.webapp.studies.rating;

/**
 * @author bkang
 */
public class ResultRating {

    final public int resultId;

    final public int userId;

    final public RatingMetric metric;

    final public double value;

    public ResultRating(int resultId, int userId, RatingMetric metric, double value) {
        this.resultId = resultId;
        this.userId = userId;
        this.metric = metric;
        this.value = value;
    }

}
