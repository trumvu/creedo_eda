package de.unibonn.creedo.webapp.studies.result;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import de.unibonn.creedo.webapp.dashboard.mining.MiningDashboardModel;
import de.unibonn.creedo.webapp.studies.PatternBuilder;
import de.unibonn.creedo.webapp.studies.PatternToBuilderConverter;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.patternset.PatternSet;

/**
 * @author bkang
 */
public class PatternSetResultCreator implements ResultCreator {

	@Override
	public List<PatternBuilder> getPatternBuilders(
			MiningDashboardModel miningDashboardModel) {
		List<Pattern> patterns = miningDashboardModel.getResutlPatterns();
		List<PatternBuilder> patternBuilders = new ArrayList<>();
		patternBuilders.add(PatternToBuilderConverter
				.getBuilder(new PatternSet(miningDashboardModel
						.getPropositionalLogic(), new HashSet<>(patterns))));
		return patternBuilders;
	}

	@Override
	public List<Integer> getListOfSecondsUntilSaved(
			MiningDashboardModel miningDashboardModel) {
        List<Integer> result = new ArrayList<>();
		result.add(Collections.max(miningDashboardModel.getSecondsUntilSaved()));
        return result;
	}

}
