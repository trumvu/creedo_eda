package de.unibonn.creedo.webapp.studies;

/**
 * User: bjacobs
 * Date: 31.10.14
 * Time: 13:11
 */

public class ResultDataContainer {
	private int resultId;
	private int sessionId;
	private String resultBuilderContent;

	public ResultDataContainer() {
	}

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public int getResultId() {
		return resultId;
	}
	public void setResultId(int resultId) {
		this.resultId = resultId;
	}

	public String getResultBuilderContent() {
		return resultBuilderContent;
	}

	public void setResultBuilderContent(String resultBuilderContent) {
		this.resultBuilderContent = resultBuilderContent;
	}
}
