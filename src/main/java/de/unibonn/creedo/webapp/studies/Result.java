package de.unibonn.creedo.webapp.studies;

/**
 * User: bjacobs
 * Date: 31.10.14
 * Time: 13:07
 */

public class Result {
	private final int resultId;
	private final PatternBuilder patternBuilder;

	public Result(int resultId, PatternBuilder patternBuilder) {
		this.resultId = resultId;
		this.patternBuilder = patternBuilder;
	}

	public int getResultId() {
		return resultId;
	}

	public PatternBuilder getPatternBuilder() {
		return patternBuilder;
	}
}
