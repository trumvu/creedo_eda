package de.unibonn.creedo.webapp.studies.result;

import java.util.List;

import de.unibonn.creedo.webapp.dashboard.mining.MiningDashboardModel;
import de.unibonn.creedo.webapp.studies.PatternBuilder;

/**
 * Interface of objects that extract a list of study results from a mining dashboard.
 *
 * @author bkang, bjacobs, mboley
 */
public interface ResultCreator {

    public List<PatternBuilder> getPatternBuilders(MiningDashboardModel miningDashboardModel);

    public List<Integer> getListOfSecondsUntilSaved(MiningDashboardModel miningDashboardModel);
}
