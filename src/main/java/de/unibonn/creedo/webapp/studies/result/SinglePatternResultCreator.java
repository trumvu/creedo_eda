package de.unibonn.creedo.webapp.studies.result;

import java.util.List;

import de.unibonn.creedo.webapp.dashboard.mining.MiningDashboardModel;
import de.unibonn.creedo.webapp.studies.PatternBuilder;
import de.unibonn.creedo.webapp.studies.PatternToBuilderConverter;

/**
 * @author {User}
 */
public class SinglePatternResultCreator implements ResultCreator {

    @Override
    public List<PatternBuilder> getPatternBuilders(MiningDashboardModel miningDashboardModel) {
        return PatternToBuilderConverter.getBuilders(miningDashboardModel.getResutlPatterns());
    }

    @Override
    public List<Integer> getListOfSecondsUntilSaved(MiningDashboardModel miningDashboardModel) {
        return miningDashboardModel.getSecondsUntilSaved();
    }


}
