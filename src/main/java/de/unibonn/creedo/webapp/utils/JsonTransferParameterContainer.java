package de.unibonn.creedo.webapp.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * User: bjacobs
 * Date: 06.06.14
 * Time: 14:54
 */

public class JsonTransferParameterContainer {
    private List range;
    // private String multiplicity;
    private String type;
    private String name;
    private String description;
    private String solutionHint;
    private String dependsOnNotice;
    private List<String> values;
    private int actionId;

	private int depth;

    private boolean valid;
    private boolean active;

	public void setRange(List range) {
        this.range = range;
        // this.multiplicity = multiplicity;
    }

    public void setValid(boolean isValid) {
        this.valid = isValid;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setValues(List values) {
    	if (values==null) {
    		this.values=values;
    		return;
    	}
    	this.values = new ArrayList<String>();
    	for (Object value: values) {
    		this.values.add(String.valueOf(value));
    	}
//        this.values = values;
    }

//    public String getMultiplicity() {
//        return multiplicity;
//    }

    public boolean getValid() {
        return this.valid;
    }

    public List<String> getRange() {
        return range;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List getValues() {
        return values;
    }

    public String toString() {
        return getName() + " = " + getValues() + ", active: " + isActive() + ", valid: " + getValid();
    }

    public void setSolutionHint(String solutionHint) {
        this.solutionHint = solutionHint;
    }

    public String getSolutionHint() {
        return solutionHint;
    }

    public void setDependsOnNotice(String dependsOnNotice) {
        this.dependsOnNotice = dependsOnNotice;
    }

    public String getDependsOnNotice() {
        return dependsOnNotice;
    }

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

    public int getActionId() {
        return actionId;
    }



    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
