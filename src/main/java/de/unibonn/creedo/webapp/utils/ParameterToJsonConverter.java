/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.unibonn.creedo.webapp.utils;

import static de.unibonn.realkd.common.logger.LogChannel.DEFAULT;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DependentParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

/**
 * Utility class that produces a utility object used for the transfer of
 * parameter information from server to client. The resulting data bean can
 * easily be converted to JSON by the framework.
 * 
 * To enable a new ui control element of a parameter type, make sure changing *all* the following places accordingly:
 * 1. ParameterType, ParameterToJsonConverter.convert() in this class.
 * 2. static/repositoryAdministratioPage.jsp for enabling new ui element in repository admin page.
 * 3. client/js/parameterHandler.js method getParameterStateWithActionId() which repsposes to the changes in repositoryAdministratioPage.
 * 4. client/js/parameterHandler.js method renderParameters() for enabling new ui element in custom mining dashboard.
 * 5. client/js/customDashboard.js method initializeParameters() to enable the change listener in custom mining dashboard. 
 * 6. client/js/mineButtonREF.js method updateParameterArea() due the code legacy. 
 * 
 * @author bjacobs
 */

public class ParameterToJsonConverter {

	public static JsonTransferParameterContainer convert(Parameter parameter,
			int depth) {
		JsonTransferParameterContainer container = new JsonTransferParameterContainer();

		if (parameter instanceof RangeEnumerableParameter) {
			// Handle range info
			RangeEnumerableParameter rangeParam = (RangeEnumerableParameter) parameter;
			// if (rangeParam.isContextValid()) {
			List<String> rangeStringList = new ArrayList<>();

			List range;
			if (rangeParam.getRange() == null) {
				range = null;
			} else {
				range = new ArrayList<>(rangeParam.getRange());
			}

			if (range.size() == 0) {
				DEFAULT.log("mining_parameter",
						"Concrete range is empty. No option was applicable.",
						LogMessageType.DEBUG_MESSAGE);
			} else {
				// Otherwise set first applicable option as initial
				// value if not set already
				if (!rangeParam.isValid()) {
					rangeParam.set(range.get(0));
				}
			}

			for (Object obj : range) {
				rangeStringList.add(obj.toString());
			}
			container.setType(ParameterType.RANGE_ENUMERABLE.getName());
			container.setRange(rangeStringList);
			ArrayList<Object> value = new ArrayList<>();
			value.add(parameter.getCurrentValue());
			container.setValues(value);
		} else if (parameter instanceof SubCollectionParameter) {
			SubCollectionParameter<?, ?> rangeParam = (SubCollectionParameter<?, ?>) parameter;

			// Handle range info
			List<String> rangeStringList = new ArrayList<>();
			for (Object x : rangeParam.getCollection()) {
				rangeStringList.add(x.toString());
			}

			if (rangeParam.getCurrentValue() == null) {
				container.setValues(null);
			} else {
				container.setValues(new ArrayList<>(rangeParam
						.getCurrentValue()));
			}
			container.setRange(rangeStringList);
			//if (parameter instanceof CustomSublistParameter) {
				container.setType(ParameterType.SUBSET.getName());
			//}
		} else {
			if (parameter instanceof DefaultParameter) {
				container.setType(ParameterType.TEXT.getName());
			}
			ArrayList<Object> value = new ArrayList<>();
			value.add(parameter.getCurrentValue());
			container.setValues(value);
		}

		// Handle active and valid info
		if (parameter instanceof DependentParameter) {
			DependentParameter dependentParam = (DependentParameter) parameter;
			container.setActive(dependentParam.isContextValid());

			String notice = generateDependsOnNotice(dependentParam
					.getDependsOnParameters());
			container.setDependsOnNotice(notice);

		}

		// Handle general info
		container.setName(parameter.getName());
		container.setDescription(parameter.getDescription());
		// container.setValues(parameter.getCurrentValue());
		container.setValid(parameter.isValid());
		container.setSolutionHint(parameter.getValueCorrectionHint());
		container.setDepth(depth);

		return container;
	}

	private static String generateDependsOnNotice(
			@SuppressWarnings("rawtypes") List<Parameter> dependsOnParameters) {
		if (dependsOnParameters == null || dependsOnParameters.size() == 0) {
			return "";
		}

		StringBuilder sb = new StringBuilder();
		sb.append("Depends on valid values for: ");
		@SuppressWarnings("rawtypes")
		Iterator<Parameter> it = dependsOnParameters.iterator();
		while (it.hasNext()) {
			Parameter<?> param = it.next();
			sb.append(param.getName());

			if (it.hasNext()) {
				sb.append(", ");
			}
		}
		return sb.toString();
	}

	public static List<JsonTransferParameterContainer> convertFlat(
			List<Parameter> parameters) {
		List<JsonTransferParameterContainer> result = new ArrayList<>();

		if (parameters != null) {
			for (Parameter parameter : parameters) {
				result.add(convert(parameter, 0));
			}
		}
		return result;
	}

	public static List<JsonTransferParameterContainer> convertRecursively(
			List<Parameter> parameters) {
		List<JsonTransferParameterContainer> result = new ArrayList<>();

		recurse(parameters, result, 0);

		return result;
	}

	private static List<JsonTransferParameterContainer> recurse(
			List<Parameter> parameters,
			List<JsonTransferParameterContainer> result, int depth) {

		if (parameters != null) {
			for (Parameter parameter : parameters) {
				result.add(convert(parameter, depth));

				if (parameter.getCurrentValue() instanceof ParameterContainer) {
					ParameterContainer container = (ParameterContainer) parameter
							.getCurrentValue();
					List<Parameter> containerParams = container
							.getTopLevelParameters();

					recurse(containerParams, result, depth + 1);
				}
			}
		}
		return result;
	}

	private enum ParameterType {
		TEXT {
			@Override
			public String getName() {
				return "Text";
			}
		},
		RANGE_ENUMERABLE {
			@Override
			public String getName() {
				return "RangeEnumerable";
			}
		},
		SUBSET {
			@Override
			public String getName() {
				return "Subset";
			}
		},
		SUBLIST {
			@Override
			public String getName() {
				return "Sublist";
			}
		};
		public abstract String getName();
	}

}
