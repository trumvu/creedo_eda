package de.unibonn.creedo.webapp.dashboard.closehandlers;

import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.mining.MiningDashboardModel;

public class MinMaxMinedPatternsCloseHanderOperation implements CloseOperation {

    private final int minimum;
	private final int maximum;

	public MinMaxMinedPatternsCloseHanderOperation(int minimum, int maximum) {
        this.minimum = Math.min(minimum, maximum);
		this.maximum = maximum;
	}

	@Override
	public void apply(DashboardModel dashboardModel) {
		;
	}

	@Override
	public String getMessage() {
		return (minimum == maximum) ?
				"Please select exactly " + minimum +  " result patterns." :
				"Please select " + minimum + " to " + maximum + " result patterns.";
	}

	@Override
	public boolean isApplicable(DashboardModel dashboardModel) {
		if (!(dashboardModel instanceof MiningDashboardModel)) {
            throw new IllegalArgumentException(
                    "can only be applied to mining dashboard");
        }
		int numberOfPatterns = ((MiningDashboardModel) dashboardModel).getResults().size();

		return (numberOfPatterns >= minimum) && (numberOfPatterns <= maximum);
	}

}
