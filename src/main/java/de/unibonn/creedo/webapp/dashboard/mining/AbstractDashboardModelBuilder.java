package de.unibonn.creedo.webapp.dashboard.mining;

import de.unibonn.creedo.webapp.dashboard.CloseHandler;
import de.unibonn.creedo.webapp.dashboard.AnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.closehandlers.GeneralCloseHandler;

/**
 * @author bjacobs, bokang
 */
public abstract class AbstractDashboardModelBuilder implements AnalyticsDashboardBuilder {
	
	protected CloseHandler closeHandler;

	@Override
	public AnalyticsDashboardBuilder setCloseHandler(CloseHandler closeHandler) {
		this.closeHandler = closeHandler;
		return this;
	}

	protected void fixCloseHandler() {
		if (closeHandler == null) {
			closeHandler = GeneralCloseHandler.DEFAULT;
		}
	}

}
