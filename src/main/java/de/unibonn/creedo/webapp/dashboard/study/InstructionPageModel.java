package de.unibonn.creedo.webapp.dashboard.study;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.servlet.ModelAndView;

import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.webapp.dashboard.CloseHandler;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;

/**
 * @author bjacobs
 * 
 */
public class InstructionPageModel implements DashboardModel {
	private final ModelAndView view;
	private final int id;
	private final CloseHandler closeHandler;
	private final BindingAwareModelMap model;

	InstructionPageModel(int id, HttpSession session, CloseHandler closeHandler, String instructionHtml) {
		
		model = new BindingAwareModelMap();

		model.addAttribute("instructionHtml", instructionHtml);

		this.view = new ModelAndView("studies/studyDisplay", model);
		this.id=id;
//		this.id = session.getId() + System.currentTimeMillis();
		this.closeHandler = closeHandler;
	}

	@Override
	public void tearDown() {
		;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public ModelAndView getModelAndView() {
		return view;
	}

	@Override
	public DashboardModel getDashboard(int id) {
		if (id==this.id) {

//		if (id.equals(this.id)) {
			return this;
		} else {
			return null;
		}
	}

	@Override
	public String getView() {
		return "studies/studyDisplay";
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}
}
