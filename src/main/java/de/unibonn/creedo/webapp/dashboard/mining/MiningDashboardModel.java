package de.unibonn.creedo.webapp.dashboard.mining;

import static de.unibonn.realkd.common.logger.LogChannel.UI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.servlet.ModelAndView;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.WebConstants;
import de.unibonn.creedo.ui.IntrospectionPage;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionLink;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.PageContainer;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.mining.AnalyticsDashboard;
import de.unibonn.creedo.ui.standard.Navbar;
import de.unibonn.creedo.webapp.dashboard.CloseHandler;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.PatternContainer;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.creedo.webapp.handler.TimeoutHandler;
import de.unibonn.creedo.webapp.patternviews.CandidatePatternGenerator;
import de.unibonn.creedo.webapp.patternviews.ResultPatternGenerator;
import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;
import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.patterns.Pattern;

/**
 * Aggregates all the state of a visual mining dashboard.
 * 
 * @author mboley, bjacobs
 * 
 */
public class MiningDashboardModel implements PatternContainer, DashboardModel,
		ActionProvider {

	private final int id;
	private final HttpSession httpSession;

	private final long instantiationTimeTag;

	private class PatternIdGenerator {
		private int id = 0;

		public synchronized int getNextId() {
			return id++;
		}
	}

	private final HashMap<Integer, WebPattern> idToWebPattern;
	private final HashMap<Pattern, WebPattern> patternToWebPattern;
	private final PatternIdGenerator patternIdGenerator;
	private final HashMap<Pattern, Long> patternToMillisecondsUntilSaved;

	private final DeveloperViewModel developerViewModel;
	private final CloseHandler closeHandler;
	private final Navbar navbar;
	private final AnalyticsDashboard analyticsDashboard;
	private final List<UiComponent> components;

	MiningDashboardModel(int id, DataWorkspace dataWorkspace,
			final HttpSession httpSession, MiningSystem miningSystem,
			CloseHandler closeHandler, DeveloperViewModel developerViewModel) {

		UI.log("Creating mining dashboard",
				LogMessageType.INTER_COMPONENT_MESSAGE);

		// this.dataTub = dataWorkspace;

		this.developerViewModel = developerViewModel;
		this.closeHandler = closeHandler;
		this.httpSession = httpSession;
		this.id = id;

		this.idToWebPattern = new HashMap<>();
		this.patternToWebPattern = new HashMap<>();
		this.patternIdGenerator = new PatternIdGenerator();

		// this.miningSystem = miningSystem;

		this.instantiationTimeTag = System.currentTimeMillis();
		this.patternToMillisecondsUntilSaved = new HashMap<>();

		this.analyticsDashboard = new AnalyticsDashboard(Creedo
				.getCreedoSession(httpSession).getUiRegister().getNextId(),
				httpSession, dataWorkspace, miningSystem);

		List<ActionLink> navbarLinks = new ArrayList<>();
		navbarLinks.add(new ActionLink(new Action() {

			private final int id = Creedo.getCreedoSession(httpSession)
					.getUiRegister().getNextId();

			@Override
			public String getReferenceName() {
				return "Help";
			}

			@Override
			public ResponseEntity<String> activate(String... params) {
				return new ResponseEntity<String>("showFrame.htm?frameId="
						+ Creedo.getCreedoSession(httpSession)
								.getHelpFrame().getId(), HttpStatus.OK);
			}

			@Override
			public ClientWindowEffect getEffect() {
				return Action.ClientWindowEffect.POPUP;
			}

			@Override
			public int getId() {
				return id;
			}
		}));

		if (Creedo.getCreedoSession(httpSession).getUser().isDeveloper()) {
			navbarLinks.add(new ActionLink(new Action() {

				private final int id=Creedo.getCreedoSession(httpSession)
						.getUiRegister().getNextId();
				
				@Override
				public String getReferenceName() {
					return "Introspection";
				}

				@Override
				public ResponseEntity<String> activate(String... params) {
					List<Action> empty = Arrays.asList();
					PageContainer pageContainer = new PageContainer(
							Creedo.getCreedoSession(httpSession)
									.getUiRegister().getNextId());
					Frame introFrame = Creedo
							.getCreedoSession(httpSession).getUiRegister()
							.createDefaultFrame(pageContainer, empty, empty);
					pageContainer.loadPage(new IntrospectionPage(
							getDeveloperViewModel()));
					return new ResponseEntity<String>("showFrame.htm?frameId="
							+ introFrame.getId(), HttpStatus.OK);
				}

				@Override
				public ClientWindowEffect getEffect() {
					return Action.ClientWindowEffect.POPUP;
				}

				@Override
				public int getId() {
					return id;
				}
			}));
		}

		navbarLinks.add(new ActionLink(new CloseAction(httpSession,
				closeHandler, this)));

		this.navbar = new Navbar(Creedo.getCreedoSession(httpSession)
				.getUiRegister().getNextId(), navbarLinks);

		addTimeoutHandler(httpSession);

		this.components = Arrays.asList((UiComponent) navbar,
				(UiComponent) analyticsDashboard);

	}

	private void addTimeoutHandler(HttpSession session) {
		TimeoutHandler timeoutHandler = new TimeoutHandler(session);
		// Attach Timeout-handler
		session.setAttribute(WebConstants.TIMEOUT_HANDLER_KEY, timeoutHandler);
	}

	/**
	 * 
	 * @param patternId
	 *            of WebPattern currently in WebDiscoveryProcessState
	 */
	public void saveToResults(int patternId) {
		long timeUntilSaved = System.currentTimeMillis() - instantiationTimeTag;
		WebPattern webPattern = idToWebPattern.get(patternId);
		webPattern.setHtmlGenerator(ResultPatternGenerator.INSTANCE);

		patternToMillisecondsUntilSaved.put(webPattern.getPattern(),
				timeUntilSaved);

		this.analyticsDashboard.getMiningSystem().getDiscoveryProcess()
				.addCandidateToResults(webPattern.getPattern());
	}

	public void discardFromResults(int patternId) {
		patternToMillisecondsUntilSaved.remove(patternId);
		WebPattern webPattern = idToWebPattern.get(patternId);
		this.analyticsDashboard.getMiningSystem().getDiscoveryProcess()
				.deletePatternFromResults(webPattern.getPattern());
	}

	public void discardFromCandidates(int patternId) {
		WebPattern webPattern = idToWebPattern.get(patternId);
		this.analyticsDashboard.getMiningSystem().getDiscoveryProcess()
				.deletePatternFromCandidates(webPattern.getPattern());
	}

	public boolean isInResults(WebPattern p) {
		return this.analyticsDashboard.getMiningSystem().getDiscoveryProcess()
				.getDiscoveryProcessState().isInResults(p.getPattern());
	}

	private List<WebPattern> compileWebPatternList(List<Pattern> patterns) {
		ArrayList<WebPattern> result = new ArrayList<>(patterns.size());
		for (Pattern pattern : patterns) {
			result.add(patternToWebPattern.get(pattern));
		}
		return result;
	}

	public List<WebPattern> getResults() {
		return compileWebPatternList(this.analyticsDashboard.getMiningSystem()
				.getDiscoveryProcess().getDiscoveryProcessState()
				.getResultPatterns());
	}

	public List<Pattern> getResutlPatterns() {
		return this.analyticsDashboard.getMiningSystem().getDiscoveryProcess()
				.getDiscoveryProcessState().getResultPatterns();
	}

	public List<WebPattern> getDiscarded() {
		return compileWebPatternList(this.analyticsDashboard.getMiningSystem()
				.getDiscoveryProcess().getDiscoveryProcessState()
				.getDiscardedPatterns());
	}

	private String getCandidatePatternsAsHTML() {
		StringBuilder html = new StringBuilder();
		for (WebPattern webPattern : compileWebPatternList(this.analyticsDashboard
				.getMiningSystem().getDiscoveryProcess()
				.getDiscoveryProcessState().getCandidatePatterns())) {
			html.append(webPattern.getHtml(httpSession));
		}

		return html.toString();
	}

	/**
	 * only to be called by MineButtonStrategies
	 */
	List<MiningAlgorithm> getAlgorithms() {
		return analyticsDashboard.getMiningSystem().getAlgorithms();
	}

	public void requestStopOfAlgorithm() {
		this.analyticsDashboard.getMiningSystem().requestStopOfAlgorithm();
	}

	/**
	 * Starts a new discovery round with the patterns provided as candidates.
	 * <p/>
	 * During a discovery round, calling the nextRound method will end the
	 * current round and start the next round automatically.
	 * <p/>
	 * When the current round is ended it can be started again by calling
	 * nextRound method.
	 *
	 * @param nextRoundPatterns
	 *            patterns to be used in the new round
	 */
	public void nextRound(List<Pattern> nextRoundPatterns) {
		removeOldCandidatesFromMaps();
		addNewCandidatesToMaps(nextRoundPatterns);
		this.analyticsDashboard.getMiningSystem().getDiscoveryProcess()
				.nextRound(nextRoundPatterns);
	}

	public void startAlgorithm(MiningAlgorithm algorithm) {
		analyticsDashboard.getMiningSystem().startAlgorithm(algorithm);
	}

	private void removeOldCandidatesFromMaps() {
		List<Integer> idsToRemove = new ArrayList<>();
		for (Entry<Integer, WebPattern> entry : idToWebPattern.entrySet()) {
			if (this.analyticsDashboard.getMiningSystem().getDiscoveryProcess()
					.getDiscoveryProcessState()
					.isInCandidates(entry.getValue().getPattern())) {
				idsToRemove.add(entry.getKey());
			}
		}
		for (Integer id : idsToRemove) {
			patternToWebPattern.remove(idToWebPattern.get(id));
			idToWebPattern.remove(id);
		}
	}

	private void addNewCandidatesToMaps(List<Pattern> nextRoundPatterns) {
		for (Pattern pattern : nextRoundPatterns) {
			WebPattern webPattern = new WebPattern(this,
					this.patternIdGenerator.getNextId(), pattern,
					CandidatePatternGenerator.INSTANCE);
			idToWebPattern.put(webPattern.getId(), webPattern);
			patternToWebPattern.put(pattern, webPattern);
		}
	}

	public PropositionalLogic getPropositionalLogic() {
		return analyticsDashboard.getPropositionalLogic();
	}

	public DeveloperViewModel getDeveloperViewModel() {
		return developerViewModel;
	}

	/**
	 * frees up resources used by AnalysisSession objects like, e.g., running
	 * mining algorithms
	 */
	@Override
	public void tearDown() {
		UI.log("Stopping currently running algorithm (if applicable)",
				LogMessageType.GLOBAL_STATE_CHANGE);
		analyticsDashboard.getMiningSystem().requestStopOfAlgorithm();

		Object timeoutHandlerObject = httpSession
				.getAttribute(WebConstants.TIMEOUT_HANDLER_KEY);

		if (timeoutHandlerObject instanceof TimeoutHandler) {
			((TimeoutHandler) timeoutHandlerObject).stop();
		}
	}

	@Override
	public int getId() {
		return id;
	}

	public String mineClicked(HttpServletRequest request)
			throws RuntimeException {
		// miningSystem.mineClicked(request, this);
		List<Pattern> minedPatterns = analyticsDashboard.getMiningSystem()
				.mineClicked(request, this);
		nextRound(minedPatterns);
		return this.getCandidatePatternsAsHTML();
	}

	@Override
	public ModelAndView getModelAndView() {
		return new ModelAndView(getView(), getModel().asMap());
	}

	@Override
	public DashboardModel getDashboard(int id) {
		if (id == this.id) {
			// if (id.equals(this.id)) {
			return this;
		} else {
			return null;
		}
	}

	@Override
	public WebPattern getPattern(int id) {
		WebPattern result = idToWebPattern.get(id);
		if (result == null) {
			throw new IllegalArgumentException(
					"MiningDashboard does not contain pattern with matching id");
		}
		return result;
	}

	public long getMillisecondsUntilSaved(Pattern pattern) {
		Long result = patternToMillisecondsUntilSaved.get(pattern);
		if (result == null) {
			throw new IllegalArgumentException(
					"MiningDashboard does not contain requested pattern");
		}

		return result;
	}

	public List<Integer> getSecondsUntilSaved() {
		List<Integer> secondsUntilSaved = new ArrayList<>();

		for (Pattern pattern : getResutlPatterns()) {
			long millisecondsUntilSaved = getMillisecondsUntilSaved(pattern);
			secondsUntilSaved.add((int) millisecondsUntilSaved / 1000);
		}
		return secondsUntilSaved;
	}

	public ManualMiningModel getManualMiningModel() {
		return analyticsDashboard.getMiningSystem().getManualMiningModel();
	}

	@Override
	public Collection<Integer> getActionIds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		if (navbar.isActionAvailable(id)) {
			return navbar.performAction(id);
		}
		throw new IllegalArgumentException("Action not available");
	}

	@Override
	public boolean isActionAvailable(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getView() {
		return "static/contentPageFrame";
	}

	@Override
	public Model getModel() {
		BindingAwareModelMap model = new BindingAwareModelMap();

		model.addAttribute("controllerName", "dashboard");
		model.addAttribute("showHeaderBar", false);

		model.addAllAttributes(navbar.getModel().asMap());
		model.addAllAttributes(analyticsDashboard.getModel().asMap());

		model.addAttribute("ping", true);

		model.addAttribute(DashboardModel.DASHBOARD_ID_ATTRIBUTENAME, getId());
		model.addAttribute("frameId", getId());
		model.addAttribute("components", components);
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return components;
	}

}
