package de.unibonn.creedo.webapp.dashboard.mining.rankerfactories;

import de.unibonn.creedo.webapp.dashboard.mining.rankers.DefaultRanker;
import de.unibonn.creedo.webapp.dashboard.mining.rankers.KnowledgeModelRanker;
import de.unibonn.creedo.webapp.dashboard.mining.rankers.Ranker;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;

public class DefaultRankerFactory implements RankerFactory {

	@Override
	public Ranker getRanker(DataTable dataTable,
			DiscoveryProcess discoveryProcess) {
		return new DefaultRanker();
	}
	
	@Override
	public boolean equals(Object other) {
		return (other instanceof DefaultRankerFactory);
	}
	
	@Override
	public String toString() {
		return "Default ranker";
	}

}
