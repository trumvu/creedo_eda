package de.unibonn.creedo.webapp.dashboard.closehandlers;

import de.unibonn.creedo.webapp.dashboard.DashboardModel;

/**
 * Interface for close operations that can be aggregated by GeneralCloseHandler.
 * 
 * @see GeneralCloseHandler
 * 
 * @author bkang, bjacobs, mboley
 * 
 */
public interface CloseOperation {

	/**
	 * Checks whether internal conditions of the operation is satisfied.
	 * If not it, the client of the operation show the @see getMessage
	 * description to the user
	 * @return true, iff this operation's condition is ok, false otherwise.
	 */
	public boolean isApplicable(DashboardModel dashboardModel);

	/**
	 * To be called if isApplicable returned true. Will apply the
	 * operation using the DashboardModel
	 */
	public void apply(DashboardModel dashboardModel);

	/**
	 * Can be called (not only) if the isApplicable method returned false.
	 * It returns a message describing what has to be done in order to
	 * fulfill the operation successfully.
	 * @return Message describing what has to be done to fulfill the operation.
	 */
	public String getMessage();
	
}
