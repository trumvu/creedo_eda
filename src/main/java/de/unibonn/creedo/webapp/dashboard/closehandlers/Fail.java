package de.unibonn.creedo.webapp.dashboard.closehandlers;


public class Fail implements CloseHandlerResponse {

    private final String message;

    public Fail(String message) {
        this.message = message;
    }

    @Override
    public boolean isSuccess() {
        return false;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
