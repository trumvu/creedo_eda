package de.unibonn.creedo.webapp.dashboard.mining;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;
import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.algorithms.MiningAlgorithmFactory;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;

/**
 * Builder creating mining system instance configured as manual mining system
 * with a dialog for algorithm selection and parameter setting.
 *
 * @author bjacobs
 * 
 */
public class ManualMiningSystemBuilder extends
		MiningSystemBuilder<ManualMiningSystemBuilder> {

	public ManualMiningSystemBuilder() {
		super();
	}

	@Override
	protected MiningSystem concreteBuild(DeveloperViewModel developerViewModel,
			DataTable table, DataWorkspace dataWorkspace) {

		List<MiningAlgorithm> algorithms = new ArrayList<>();
		for (MiningAlgorithmFactory factory : getAlgorithmFactories()) {
			algorithms.add(factory.create(dataWorkspace));
		}

		DiscoveryProcess discoveryProcess = new DiscoveryProcess();

		// System.out.println("BUILDING MINING SYSTEM WITH: " +
		// this.getRankerFactory());

		return new MiningSystem(algorithms, discoveryProcess,
				MineButtonStrategy.MANUALMINING, super.getRankerFactory()
						.getRanker(table, discoveryProcess));

	}

	@Override
	public String toString() {
		return "Manual mining system";
	}

}
