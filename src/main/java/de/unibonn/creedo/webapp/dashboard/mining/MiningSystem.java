package de.unibonn.creedo.webapp.dashboard.mining;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;

import static de.unibonn.realkd.common.logger.LogChannel.DISCOVERY_PROCESS;
import static de.unibonn.realkd.common.logger.LogChannel.UI;
import de.unibonn.creedo.webapp.dashboard.mining.rankers.Ranker;
import de.unibonn.creedo.webapp.viewmodels.MetaDataTableModel;
import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.algorithms.StoppableMiningAlgorithm;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;
import de.unibonn.realkd.patterns.Pattern;

/**
 * Class representing a mining system
 */
public class MiningSystem {
	private final List<MiningAlgorithm> algorithms;
	private final DiscoveryProcess discoveryProcess;
	private final MineButtonStrategy strategy;
	private final Ranker ranker;

	private List<Pattern> minedPatterns;

	private Future<Collection<Pattern>> future;
	private MiningAlgorithm currentAlgorithm;

	/**
	 * Only to be called from MiningSystemBuilder
	 */
	public MiningSystem(List<MiningAlgorithm> algorithms,
			DiscoveryProcess discoveryProcess, MineButtonStrategy strategy, Ranker ranker) {
		this.algorithms = algorithms;
		this.discoveryProcess = discoveryProcess;
		this.strategy = strategy;
		this.ranker = ranker;
		this.manualMiningModel = new ManualMiningModel(algorithms);
	}

	public List<MiningAlgorithm> getAlgorithms() {
		return algorithms;
	}

	public MineButtonStrategy getMineButtonStrategy() {
		return strategy;
	}

	public DiscoveryProcess getDiscoveryProcess() {
		return discoveryProcess;
	}

	public List<Pattern> mineClicked(HttpServletRequest request, MiningDashboardModel miningDashboardModel)
			throws RuntimeException {
		strategy.clicked(this, request, algorithms);
        return minedPatterns;
	}
	
	private final ManualMiningModel manualMiningModel;
	
	public ManualMiningModel getManualMiningModel() {
		return manualMiningModel;
	}

	public void requestStopOfAlgorithm() {
		UI.log("Requesting currently run algorithm to stop",
				LogMessageType.INTER_COMPONENT_MESSAGE);
		if (currentAlgorithm != null) {
			getDiscoveryProcess().endRound();

			if (currentAlgorithm instanceof StoppableMiningAlgorithm) {
				((StoppableMiningAlgorithm) currentAlgorithm).requestStop();
			}
		}
	}

	public void startNextDiscoveryRoundWithCurrentAlgorithmResult()
			throws RuntimeException {
		List<Pattern> patterns = new ArrayList<>();
		try {
			patterns.addAll(future.get());
		} catch (InterruptedException | ExecutionException e) {
			DISCOVERY_PROCESS.log(
					"Exception while receiving result of algorithm: "
							+ e.getMessage(),
					LogMessageType.GLOBAL_STATE_CHANGE);
			e.printStackTrace();

			future.cancel(true);
			future = null;

			throw new RuntimeException("Algorithm crashed: " + e.getMessage(),
					e);

			// System.exit(-1);
		}
		minedPatterns = ranker.rank(patterns);
	}

	public void startAlgorithm(MiningAlgorithm algorithm) {
		UI.log("starting mine service", LogMessageType.INTER_COMPONENT_MESSAGE);

		future = Executors.newSingleThreadExecutor().submit(algorithm);

		currentAlgorithm = algorithm;
	}


}
