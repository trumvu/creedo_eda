package de.unibonn.creedo.webapp.dashboard.mining;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

import de.unibonn.creedo.common.DefaultSubCollectionParameter;
import de.unibonn.creedo.common.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.creedo.webapp.dashboard.mining.rankerfactories.DefaultRankerFactory;
import de.unibonn.creedo.webapp.dashboard.mining.rankerfactories.KnowledgeModelRankerFactory;
import de.unibonn.creedo.webapp.dashboard.mining.rankerfactories.RankerFactory;
import de.unibonn.creedo.webapp.utils.serialization.AlgorithmFactory;
import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;
import de.unibonn.realkd.common.parameter.AbstractNonEmptySubListParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.table.DataTable;

/**
 * Abstract class for builders of MiningSystems that creates MiningAlgorithms
 * and passes them to concrete subclasses via the concretelyBuild-hook.
 * 
 * @author bjacobs
 * 
 */
public abstract class MiningSystemBuilder<T> implements ParameterContainer {

	private static List<RankerFactory> rankerFactoryOptions = Arrays.asList(
			new DefaultRankerFactory(), new KnowledgeModelRankerFactory());

//	private static class AlgorithmParameter extends
//			AbstractNonEmptySubListParameter<AlgorithmFactory> {
//
//		public AlgorithmParameter() {
//			super(
//					"Algorithms",
//					"The algorithms that will be available in the mining system",
//					List.class, Arrays.asList(), "Select non-empty sublist");
//		}
//
//		@Override
//		protected List<AlgorithmFactory> getConcreteRange() {
//			return Arrays.asList(AlgorithmFactory.values());
//		}
//
//	}

	// private List<AlgorithmFactory> algorithmFactories;

	private final Parameter<List<AlgorithmFactory>> algorithms;

	// protected RankerFactory rankerFactory;

	private final Parameter<RankerFactory> postProcessor;

	protected final DefaultParameterContainer parameterContainer;

	public MiningSystemBuilder() {
		// this.algorithms = new AlgorithmParameter();
		this.algorithms = DefaultSubCollectionParameter
				.getDefaultSubListParameter(
				"Algorithms",
				"The algorithms that will be available in the mining system",
				new CollectionComputer<List<AlgorithmFactory>>() {
					@Override
					public List<AlgorithmFactory> computeCollection() {
						return Arrays.asList(AlgorithmFactory.values());
					}

				});
		this.parameterContainer = new DefaultParameterContainer(
				"mining system builder");
		this.postProcessor = new DefaultRangeEnumerableParameter<RankerFactory>(
				"Post processor",
				"A post-processor which is applied to the output of any mining algorithm started with the system.",
				RankerFactory.class, new RangeComputer<RankerFactory>() {
					@Override
					public List<RankerFactory> computeRange() {
						return rankerFactoryOptions;
					}
				});

		this.parameterContainer.addAllParameters(Arrays.asList(algorithms,
				postProcessor));

		// this.postProcessor = new DefaultParameter<RankerFactory>(
		// "Post processor",
		// "A post-processor which is applied to the output of any mining algorithm started with the system.",
		// RankerFactory.class);

	}

	public final MiningSystem build(DeveloperViewModel developerViewModel,
			DataTable table, DataWorkspace dataWorkspace) {
		fixRankerFactory();
		return concreteBuild(developerViewModel, table, dataWorkspace);
	}

	protected abstract MiningSystem concreteBuild(
			DeveloperViewModel developerViewModel, DataTable table,
			DataWorkspace dataWorkspace);

	public T setRankerFactory(RankerFactory rankerFactory) {
		this.postProcessor.set(rankerFactory);
		// this.rankerFactory = rankerFactory;
		return (T) this;
	}

	public RankerFactory getRankerFactory() {
		return this.postProcessor.getCurrentValue();
	}

	private void fixRankerFactory() {
		if (this.postProcessor.getCurrentValue() == null) {
			this.setRankerFactory(new DefaultRankerFactory());
		}
		// if (this.rankerFactory == null) {
		// this.rankerFactory = new DefaultRankerFactory();
		// }
	}

	public T setAlgorithmFactories(List<AlgorithmFactory> algorithmFactories) {
		this.algorithms.set(algorithmFactories);
		// this.algorithmFactories = algorithmFactories;
		return (T) this;
	}

	public List<AlgorithmFactory> getAlgorithmFactories() {
		return algorithms.getCurrentValue();
		// return algorithmFactories;
	}

	@Override
	public final boolean isStateValid() {
		return this.parameterContainer.isStateValid();
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return this.parameterContainer.findParameterByName(name);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<Parameter> getTopLevelParameters() {
		return this.parameterContainer.getTopLevelParameters();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		this.parameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(
			final Map<String, String[]> crossOutMap) {
		this.parameterContainer.unloadMapValuesToParameters(crossOutMap);
	}

//	public static MiningSystemBuilder deserializeBuilder(
//			String systemBuilderClassName, String systemBuilderContent) {
//
//		try {
//			// Load target builder class and content
//			Class<?> systemBuilderClass = Class.forName(systemBuilderClassName);
//
//			// Retrieve custom deserializer from this class
//			MiningSystemBuilder o = (MiningSystemBuilder) systemBuilderClass
//					.newInstance();
//			JsonDeserializer jsonDeserializer = o.getJsonDeserializer();
//
//			GsonBuilder gsonBuilder = new GsonBuilder();
//
//			// Register custom type adapters
//			gsonBuilder.registerTypeAdapter(systemBuilderClass,
//					jsonDeserializer);
//			Map<Class, JsonDeserializer> customTypeAdapters = o
//					.getCustomTypeAdapters();
//			for (Map.Entry<Class, JsonDeserializer> entry : customTypeAdapters
//					.entrySet()) {
//				gsonBuilder.registerTypeAdapter(entry.getKey(),
//						entry.getValue());
//			}
//
//			Gson gson = gsonBuilder.create();
//
//			// Deserialize and return configured instance
//			return (MiningSystemBuilder) gson.fromJson(systemBuilderContent,
//					systemBuilderClass);
//		} catch (ClassNotFoundException | IllegalAccessException
//				| InstantiationException e) {
//			throw new RuntimeException(
//					"Error instantiating MiningSystemBuilder with class: \""
//							+ systemBuilderClassName + "\" and content: \""
//							+ systemBuilderContent + "\"", e);
//		}
//	}
}
