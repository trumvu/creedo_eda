package de.unibonn.creedo.webapp.dashboard.closehandlers;


public interface CloseHandlerResponse {

    public boolean isSuccess();

    public String getMessage();

}
