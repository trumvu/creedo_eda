package de.unibonn.creedo.webapp.dashboard.closehandlers;

public class Success implements CloseHandlerResponse {

    private final String message;

    public Success(String message) {
        this.message = message;
    }

    @Override
    public boolean isSuccess() {
        return true;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
