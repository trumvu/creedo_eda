package de.unibonn.creedo.webapp.dashboard.mining;

import static de.unibonn.realkd.common.logger.LogChannel.DEFAULT;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.webapp.utils.JsonTransferParameterContainer;
import de.unibonn.creedo.webapp.utils.ParameterToJsonConverter;
import de.unibonn.creedo.webapp.utils.ParameterTransfer;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.common.logger.LogMessageType;

/**
 * @author bjacobs
 */
public class ManualMiningModel {

	private final List<MiningAlgorithm> algorithms;
	private final List<AlgorithmCategory> algorithmCategories;

	private MiningAlgorithm currentSelectedMiningAlgorithm;

	public ManualMiningModel(List<MiningAlgorithm> algorithms) {
		this.algorithms = algorithms;
		this.algorithmCategories = compileAlgorithmCategories();
	}

	public List<CategoryResultContainer> getCategories() {
		// Create result list
		List<CategoryResultContainer> result = new ArrayList<>();
		for (AlgorithmCategory cat : algorithmCategories) {
			String fullName = cat.getFullName();
			CategoryResultContainer newResult = new CategoryResultContainer(
					fullName, cat);
			result.add(newResult);
		}

		return result;
	}

	public List<AlgorithmResultContainer> getCategoryAlgorithms(
			AlgorithmCategory category) {
		List<MiningAlgorithm> tmpResults = getAlgorithmsByCategory(category);

		List<AlgorithmResultContainer> results = new ArrayList<>();
		for (MiningAlgorithm ad : tmpResults) {
			results.add(new AlgorithmResultContainer(ad
					.getName(), ad.getDescription()));
		}

		return results;
	}

	public void setCurrentAlgorithmByName(String algorithmName) {
		MiningAlgorithm algorithm = getMiningAlgorithmByName(algorithmName);
		DEFAULT.log("ref_system",
				"Requested algorithm with AlgorithmDefinition found: "
						+ algorithm.getName(),
				LogMessageType.INTER_COMPONENT_MESSAGE);

		currentSelectedMiningAlgorithm = algorithm;
		/*
		 * 
		 * return
		 * ParameterToJsonConverter.convertFlat(currentSelectedMiningAlgorithm
		 * .getTopLevelParameters());
		 */
	}

	public List<JsonTransferParameterContainer> getCurrentAlgorithmParameters() {
		return ParameterToJsonConverter
				.convertRecursively(currentSelectedMiningAlgorithm
						.getTopLevelParameters());
	}

	public MiningAlgorithm getMiningAlgorithmByName(String name) {
		for (MiningAlgorithm algo : algorithms) {
			if (algo.getName().equals(name)) {
				return algo;
			}
		}
		throw new IllegalArgumentException("No algorithm with name " + name
				+ " found in the set of documented algorithms.");
	}

	public List<AlgorithmCategory> getAlgorithmCategories() {
		return algorithmCategories;
	}

	/**
	 * @return list of all available algorithm categories (in deterministic
	 *         order given by category enum)
	 */
	private List<AlgorithmCategory> compileAlgorithmCategories() {
		//
		List<AlgorithmCategory> categories = new ArrayList<>();

		for (AlgorithmCategory category : AlgorithmCategory.values()) {
			for (MiningAlgorithm algorithm : algorithms) {
				if (algorithm.getCategory().equals(category)) {
					categories.add(category);
					break;
				}
			}
		}
		return categories;
	}

	public List<MiningAlgorithm> getAlgorithmsByCategory(AlgorithmCategory cat) {
		List<MiningAlgorithm> result = new ArrayList<>();

		// Filter by category
		for (MiningAlgorithm algo : algorithms) {
			if (algo.getCategory() == cat) {
				result.add(algo);
			}
		}
		return result;
	}

	public void applyAlgorithmSettings(ParameterTransfer execParamSetup) {

//		List<JsonReceiveParameterContainer> paramsValuesFromUi = execParamSetup
//				.getParameters();

		currentSelectedMiningAlgorithm.passValuesToParameters(execParamSetup
				.getParametersAsKeyValueMap());


//		// Map received values to parameters
//		for (Parameter<?> param : currentSelectedMiningAlgorithm
//				.getTopLevelParameters()) {
//			String paramIdentifier = param.getName();
//			for (JsonReceiveParameterContainer received : paramsValuesFromUi) {
//				if (received.getId().equals(paramIdentifier)) {
//					// Finally set the parameter value
//					String[] value = received.getValue();
//					DEFAULT.log("ref_system", "Received parameter value: "
//							+ Arrays.toString(value),
//							LogMessageType.DEBUG_MESSAGE);
//					try {
//						param.setByString(value);
//						DEFAULT.log(
//								"ref_system",
//								"Applied received parameter value to parameter: "
//										+ param.getName() + " -> "
//										+ Arrays.toString(value),
//								LogMessageType.INTER_COMPONENT_MESSAGE);
//					} catch (NumberFormatException e) {
//						DEFAULT.log(
//								"ref_system",
//								"UI tried to set invalid number for numeric parameter. Value ignored.",
//								LogMessageType.INTER_COMPONENT_MESSAGE);
//					} catch (IllegalArgumentException e) {
//						UI.log("Valid for '" + param
//								+ "' meanwhile became invalid",
//								LogMessageType.INTER_COMPONENT_MESSAGE);
//					}
//
//					break;
//				}
//			}
//			// }
//		}
	}

	public MiningAlgorithm getSelectedAlgorithm() {
		return currentSelectedMiningAlgorithm;
	}

	/*
	 * ResultContainer classes are used for the selection of certain properties
	 * that should be displayed to the client application through the
	 * JSON-interface. The JSON-parser only regards the public fields of
	 * classes.
	 */
	static class CategoryResultContainer {
		public String description;
		public AlgorithmCategory category;

		CategoryResultContainer(String description, AlgorithmCategory category) {
			this.description = description;
			this.category = category;
		}
	}

	static class AlgorithmResultContainer {
		public String name;
		public String description;

		AlgorithmResultContainer(String name, String description) {
			this.name = name;
			this.description = description;
		}
	}
}
