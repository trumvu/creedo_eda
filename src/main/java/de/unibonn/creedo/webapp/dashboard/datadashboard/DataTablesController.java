package de.unibonn.creedo.webapp.dashboard.datadashboard;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.mining.AnalyticsDashboard;
import de.unibonn.creedo.ui.mining.DataViewContainer;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.viewmodels.PointCloudViewModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * TODO implement server side processing for DataTables:
 * http://datatables.net/manual/server-side
 * 
 * @author elvin
 * 
 */
@Controller
public class DataTablesController {
	@Autowired
	private HttpSession session;

	@RequestMapping(value = "/getPointCloud.json", method = RequestMethod.GET)
	public @ResponseBody List<PointCloudViewModel.JsonPointCloudDataEntry> getPointCloud(
			@RequestParam(value = "dataViewContainerId") int dataViewContainerId,
			HttpServletResponse response) throws Exception {
//		CreedoSession uiSession = CreedoSession.getCreedoSession(session);
//
//		DashboardModel dashboardModel = uiSession
//				.getDashboard(analysisSessionId);
//
//		DataViewModelContainer dashboardWithDataview = (DataViewModelContainer) dashboardModel;

		UiComponent component = Creedo.getCreedoSession(session)
				.getUiComponent(dataViewContainerId);

		if (!(component instanceof DataViewContainer)) {
			throw new IllegalStateException(
					"Request does not refer to a data view container");
		}
		
		PointCloudViewModel pcViewModel = ((DataViewContainer)component)
				.getPointCloudViewModel();

		return pcViewModel.getPoints();
	}

	/**
	 * Returns the data in the DataTable in its original order. Do not change
	 * the order here since it will break the data-consistency with clients.
	 * 
	 * @return DataTable data in original order
	 * @throws Exception
	 *             Exception if requesting user is not authenticated
	 */
	@RequestMapping(value = "/getDataTable.json", method = RequestMethod.GET)
	public @ResponseBody List<List<String>> getDataTable(
			@RequestParam(value = "dataViewContainerId") int dataViewContainerId,
			HttpServletResponse response) throws Exception {

		UiComponent component = Creedo.getCreedoSession(session)
				.getUiComponent(dataViewContainerId);

		if (!(component instanceof DataViewContainer)) {
			throw new IllegalStateException(
					"Request does not refer to a data view container");
		}
		return ((DataViewContainer) component).getDataTableViewModel()
				.getDataTableContent();

	}

	// return column/attribute id, tooltip pairs
	@RequestMapping(value = "/getColumnTooltips.json", method = RequestMethod.GET)
	public @ResponseBody List<String> getColumnTooltips(
			@RequestParam(value = "dataViewContainerId") int dataViewContainerId,
			HttpServletResponse response) throws Exception {
		UiComponent component = Creedo.getCreedoSession(session)
				.getUiComponent(dataViewContainerId);

		if (!(component instanceof DataViewContainer)) {
			throw new IllegalStateException(
					"Request does not refer to a data view container");
		}
		return ((DataViewContainer) component).getDataTableViewModel()
				.getColumnToolTipHtml();
	}

}
