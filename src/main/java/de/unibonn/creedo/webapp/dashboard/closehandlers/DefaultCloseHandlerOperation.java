package de.unibonn.creedo.webapp.dashboard.closehandlers;

import de.unibonn.creedo.webapp.dashboard.DashboardModel;

public class DefaultCloseHandlerOperation implements CloseOperation {

	public static CloseOperation INSTANCE=new DefaultCloseHandlerOperation();

	private DefaultCloseHandlerOperation() {
		;
	}
	
    @Override
    public void apply(DashboardModel dashboardModel) {
        ;
    }

    @Override
    public String getMessage() {
        return "Always returns OK";
    }

	@Override
	public boolean isApplicable(DashboardModel dashboardModel) {
		return true;
	}
    
}
