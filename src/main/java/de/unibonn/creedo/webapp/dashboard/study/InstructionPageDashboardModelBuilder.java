package de.unibonn.creedo.webapp.dashboard.study;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.mining.AbstractDashboardModelBuilder;

/**
 * @author bjacobs
 */
public class InstructionPageDashboardModelBuilder extends
		AbstractDashboardModelBuilder {
	private String instructionHtml;

	public InstructionPageDashboardModelBuilder setInstructionHtml(
			String instructionHtml) {
		this.instructionHtml = instructionHtml;
		return this;
	}

	@Override
	public DashboardModel build(int frameId, HttpSession session) {
		fixCloseHandler();
		fixInstructionHtml();

		return new InstructionPageModel(frameId, session, closeHandler,
				instructionHtml);
	}

	private void fixInstructionHtml() {
		if (instructionHtml == null) {
			instructionHtml = "No instructions were provided.";
		}
	}
}
