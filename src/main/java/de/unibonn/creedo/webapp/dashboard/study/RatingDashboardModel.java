package de.unibonn.creedo.webapp.dashboard.study;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.servlet.ModelAndView;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.ActionLink;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.mining.DataViewContainer;
import de.unibonn.creedo.ui.standard.Navbar;
import de.unibonn.creedo.webapp.dashboard.CloseHandler;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.mining.CloseAction;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.PatternContainer;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.creedo.webapp.patternviews.RatingBoardPatternHTMLGenerator;
import de.unibonn.creedo.webapp.studies.Result;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import de.unibonn.creedo.webapp.studies.rating.ResultRating;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.DataWorkspaceFactory;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;

/**
 * Represents all state of a dashboard for rating patterns.
 * 
 * @author bjacobs, mboley
 */
public class RatingDashboardModel implements PatternContainer, DashboardModel,
		ActionProvider {

	private final ModelAndView view;
	private final int id;

	private DataWorkspace dataTub;
	private HttpSession httpSession;
	private final int numberOfRatingsNeedToCollect;

	private final CloseHandler closeHandler;

	private final Map<Integer, WebPattern> patterns;
	private final RatingSystem ratingSystem;
	private BindingAwareModelMap model;

	private final DataViewContainer dataViewContainer;
	private Navbar navbar;

	RatingDashboardModel(int id, List<Result> results,
			List<RatingMetric> metrics, HttpSession session,
			DataTable dataTable, CloseHandler closeHandler) {

		this.closeHandler = closeHandler;
		this.httpSession = session;
		this.id = id;
		// this.id = session.getId() + System.currentTimeMillis();

		// this.dataTable = dataTable;
		this.dataTub = DataWorkspaceFactory.INSTANCE.createDataWorkspace();// new
																			// DefaultDataWorkspace();
		this.dataTub.add(dataTable);
		// this.dataTub.add(dataTable.getPropositionStore());
		this.dataTub.add(new PropositionalLogic(dataTable));

		this.patterns = new LinkedHashMap<>();
		this.ratingSystem = new RatingSystem(metrics);

		this.numberOfRatingsNeedToCollect = metrics.size() * results.size();

		this.navbar = new Navbar(Creedo.getCreedoSession(session)
				.getUiRegister().getNextId(), Arrays.asList(new ActionLink(
				new CloseAction(httpSession, closeHandler, this))));

		this.dataViewContainer = new DataViewContainer(Creedo
				.getCreedoSession(session).getUiRegister().getNextId(),
				session, dataTub);

		for (Result result : results) {
			WebPattern webPattern = new WebPattern(this, result.getResultId(),
					// result.getPatternBuilder().build(dataTable.getPropositionStore()),
					result.getPatternBuilder().build(
							dataTub.getAllPropositionalLogics().get(0)),
					RatingBoardPatternHTMLGenerator.INSTANCE);

			patterns.put(result.getResultId(), webPattern);
		}

		List<String> resultPatternsHtml = new ArrayList<>();
		for (WebPattern pattern : patterns.values()) {
			resultPatternsHtml.add(pattern.getHtml(session));
		}
		// this.metrics = metrics;

		initRatings();

		model = new BindingAwareModelMap();

		model.addAttribute("frameId", getId());
		model.addAllAttributes(navbar.getModel().asMap());

		model.addAttribute("resultPatterns", resultPatternsHtml);
		model.addAttribute(DashboardModel.DASHBOARD_ID_ATTRIBUTENAME, id); // Legacy
																			// -
																			// to
																			// be
																			// removed!
		model.addAttribute("ratingMetrics", metrics);

		model.addAllAttributes(dataViewContainer.getModel().asMap());

		String datasetName = dataTable.getName();
		model.addAttribute("title", datasetName);
		// model.addAttribute("ping", true);

		this.view = new ModelAndView(getView(), model);
	}

	private void initRatings() {
		for (WebPattern webPattern : patterns.values()) {
			// List<RatingOption> ratingOptions = new ArrayList<>();
			// for (RatingMetric ratingMetric : metrics) {
			// ratingOptions.add(ratingMetric.getDefault());
			// }
			// ratings.put(webPattern.getId(), ratingOptions);
			ratingSystem.init(webPattern.getId());
		}
	}

	@Override
	public void tearDown() {
		;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public ModelAndView getModelAndView() {
		return view;
	}

	@Override
	public DashboardModel getDashboard(int id) {
		if (id == this.id) {
			return this;
		} else {
			return null;
		}
	}

	public void updateRating(int patternId, String metricName, int optionId) {
		ratingSystem.updateRating(patternId, metricName, optionId);
	}

	public List<Integer> getPatternRating(int patternId) {
		return ratingSystem.getPatternRating(patternId);
	}

	@Override
	public WebPattern getPattern(int id) {
		WebPattern result = patterns.get(id);
		if (result == null) {
			throw new IllegalArgumentException(
					"ratingdashboard does not contain pattern with matching id");
		}
		return result;
	}

	public List<ResultRating> getResultRatings() {
		List<ResultRating> resultRatings = new ArrayList<>();
		int userId = Creedo.getCreedoSession(httpSession).getUser()
				.getId();
		for (Integer resultId : patterns.keySet()) {
			List<RatingMetric> metrics = ratingSystem.getMetrics();
			List<Integer> ratings = ratingSystem.getPatternRating(patterns.get(
					resultId).getId());
			for (int i = 0; i < ratings.size(); i++) {
				resultRatings.add(new ResultRating(resultId, userId, metrics
						.get(i), metrics.get(i).getRatingOptions()[ratings
						.get(i)].getValue()));
			}
		}
		return resultRatings;
	}

	public boolean haveCollectedRequiredNumberOfRatings() {
		for (Integer resultId : patterns.keySet()) {
			List<Integer> ratings = ratingSystem.getPatternRating(patterns.get(
					resultId).getId());
			for (int i = 0; i < ratings.size(); i++) {
				if (ratings.get(i) == null) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public String getView() {
		return "studies/ratePattern";
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList((UiComponent) navbar,
				(UiComponent) dataViewContainer);
	}

	@Override
	public Collection<Integer> getActionIds() {
		return navbar.getActionIds();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		return navbar.performAction(id, params);
	}

	@Override
	public boolean isActionAvailable(int id) {
		return navbar.isActionAvailable(id);
	}
}
