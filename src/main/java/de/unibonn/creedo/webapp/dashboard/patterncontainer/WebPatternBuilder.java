package de.unibonn.creedo.webapp.dashboard.patterncontainer;

import javax.servlet.http.HttpSession;

import de.unibonn.realkd.data.table.DataTable;

public interface WebPatternBuilder {

	public WebPattern build(DataTable dataTable, HttpSession httpSession,
		PatternContainer dashboard);

/*
	public static WebPatternBuilder RANDOM_ASSOCIATION_BUILDER = new WebPatternBuilder() {
		int idCounter = 0;

		@Override
		public WebPattern build(DataTable dataTable, HttpSession httpSession,
				PatternContainerDashboard dashboard) {
			List<Integer> shuffledPropositionIndices = Sampling
					.getPermutation(dataTable.getPropositionStore()
							.getPropositions().size());
			List<Proposition> elements = new ArrayList<Proposition>();
			elements.add(dataTable.getPropositionStore().getPropositions()
					.get(shuffledPropositionIndices.get(0)));
			elements.add(dataTable.getPropositionStore().getPropositions()
					.get(shuffledPropositionIndices.get(1)));
			Description description = new Description(dataTable, elements);
			Association association = new Association(dataTable, description);
			// WebPattern webPattern=new WebPattern(0l, association,
			// ResultPatternGenerator.INSTANCE);
			WebPattern webPattern = new WebPattern(dashboard, idCounter++,
					association, RatingBoardPatternHTMLGenerator.INSTANCE);
			webPattern.setAnnotationText("This is a random association");
			return webPattern;
		}
	};
*/

}
