package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.creedo.webapp.patternviews.AbstractResultPatternMapper.AnnotationVisibility;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.patternset.PatternSet;

public class RatingBoardPatternHTMLGenerator implements PatternHTMLGenerator {

    public static final String SELECT_ACTION_HTML = "<span class=\"selectable\"/>";

	public static RatingBoardPatternHTMLGenerator INSTANCE = new RatingBoardPatternHTMLGenerator();

	private static List<String> ACTIONS = new ArrayList<String>();
	static {
		// ACTIONS.add(GeneralPattenGenerator.DELETION_ACTION_HTML);
        ACTIONS.add(SELECT_ACTION_HTML);
	}

	private static ResultDefaultPatternGenerator DEFAULT_GENERATOR = new ResultDefaultPatternGenerator(
			ACTIONS, AnnotationVisibility.READONLY);

	private static ResultExceptionalModelPatternMapper EMM_GENERATOR = new ResultExceptionalModelPatternMapper(
			ACTIONS, AnnotationVisibility.READONLY);

	private static ResultAssociationPatternMapper ASSOCIATION_GENERATOR = new ResultAssociationPatternMapper(
			ACTIONS, AnnotationVisibility.READONLY);

    private static ResultPatternSetMapper PATTERN_SET_GENERATOR = new ResultPatternSetMapper(ACTIONS);

	private RatingBoardPatternHTMLGenerator() {
		;
	}

	@Override
	public String getHTML(HttpSession session, WebPattern webPattern) {
		if (webPattern.getPattern() instanceof Association) {
			return RatingBoardPatternHTMLGenerator.ASSOCIATION_GENERATOR
					.getHTML(session, webPattern);
		} else if (webPattern.getPattern() instanceof ExceptionalModelPattern) {
            return RatingBoardPatternHTMLGenerator.EMM_GENERATOR.getHTML(
                    session, webPattern);
        } else if (webPattern.getPattern() instanceof PatternSet){
            return RatingBoardPatternHTMLGenerator.PATTERN_SET_GENERATOR.getHTML(session, webPattern);
		} else {
			return RatingBoardPatternHTMLGenerator.DEFAULT_GENERATOR.getHTML(
					session, webPattern);
		}
	}

}
