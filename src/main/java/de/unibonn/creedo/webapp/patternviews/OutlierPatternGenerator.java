package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.Pattern;

public class OutlierPatternGenerator extends
		AbstractCandidatePatternGenerator {
	
	public OutlierPatternGenerator(List<String> optionalActions) {
		super(optionalActions);
	}

	@Override
	protected String getHTMLClass() {
		// TODO: Is there need for an own html class here?
		return "generic-pattern";
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		return "Outlier Pattern";
	}

	@Override
	protected String getTooltip(Pattern pattern) {
		StringBuffer res=new StringBuffer();
		for (Integer index: pattern.getSupportSet()) {
			res.append("<br/>");
			res.append(pattern.getDataArtifact().getObjectName(index));
		}
		return res.toString();
	}

	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		return new ArrayList<>();
	}

//	@Override
//	protected String getDescription(Pattern pattern) {
//		StringBuffer res=new StringBuffer();
//		int numElements=0;
//		for (Integer index: pattern.getSupportSet()) {
//			res.append("<br/>");
//			res.append(pattern.getDataTable().getObjectName(index));
//			numElements++;
//			if (numElements==VISIBLE_DESCRIPTOR_NUMBER) break;
//		}
//		if (pattern.getSupportSet().size() > VISIBLE_DESCRIPTOR_NUMBER) {
//			res.append("<span id = \"hasMore\"><br/>... </span>");
//		}
//		return res.toString();
//	}
	
	@Override
	protected List<String> getDescriptorElements(Pattern pattern) {
		List<String> res=new ArrayList<>(pattern.getSupportSet().size());
		for (Integer index: pattern.getSupportSet()) {
			res.add(pattern.getDataArtifact().getObjectName(index));
		}

		return res;
	}
	
	

}
