package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.outlier.Outlier;

public class CandidatePatternGenerator implements PatternHTMLGenerator {

	public static CandidatePatternGenerator INSTANCE = new CandidatePatternGenerator();

	private static List<String> ACTIONS = new ArrayList<String>();
	static {
		ACTIONS.add(GeneralPattenGenerator.DELETION_ACTION_HTML);
	}

	private static CandidateAssociationPatternMapper ASSOCIATION_GENERATOR = new CandidateAssociationPatternMapper(
			ACTIONS);

	private static CandidateExceptionalModelPatternMapper EMM_GENERATOR = new CandidateExceptionalModelPatternMapper(
			ACTIONS);
	
	private static OutlierPatternGenerator OUTLIER_GENERATOR = new OutlierPatternGenerator(ACTIONS);

	private static CandidateDefaultPatternGenerator DEFAULT_GENERATOR = new CandidateDefaultPatternGenerator(
			ACTIONS);

	private CandidatePatternGenerator() {
		;
	}

	@Override
	public String getHTML(HttpSession session, WebPattern webPattern) {
		if (webPattern.getPattern() instanceof Association) {
			return CandidatePatternGenerator.ASSOCIATION_GENERATOR.getHTML(
					session, webPattern);
		} else if (webPattern.getPattern() instanceof ExceptionalModelPattern) {
			return CandidatePatternGenerator.EMM_GENERATOR.getHTML(session,
					webPattern);
		} else if (webPattern.getPattern() instanceof Outlier){
			return CandidatePatternGenerator.OUTLIER_GENERATOR.getHTML(session, webPattern);
		} else {
			return CandidatePatternGenerator.DEFAULT_GENERATOR.getHTML(session,
					webPattern);
		}
	}
}
