package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.outlier.Outlier;

public class ResultPatternGenerator implements PatternHTMLGenerator {

	public static ResultPatternGenerator INSTANCE = new ResultPatternGenerator();

	private static List<String> ACTIONS = new ArrayList<String>();
	static {
		ACTIONS.add(GeneralPattenGenerator.DELETION_ACTION_HTML);
	}

	private static ResultDefaultPatternGenerator DEFAULT_GENERATOR = new ResultDefaultPatternGenerator(
			ACTIONS, AbstractResultPatternMapper.AnnotationVisibility.EDITABLE);

	private static ResultExceptionalModelPatternMapper EMM_GENERATOR = new ResultExceptionalModelPatternMapper(
			ACTIONS, AbstractResultPatternMapper.AnnotationVisibility.EDITABLE);

	private static ResultAssociationPatternMapper ASSOCIATION_GENERATOR = new ResultAssociationPatternMapper(
			ACTIONS, AbstractResultPatternMapper.AnnotationVisibility.EDITABLE);
	
	private static ResultOutlierPatternGenerator OUTLIER_GENERATOR = new ResultOutlierPatternGenerator(
			ACTIONS, AbstractResultPatternMapper.AnnotationVisibility.EDITABLE);

	private ResultPatternGenerator() {
		;
	}

	@Override
	public String getHTML(HttpSession session, WebPattern webPattern) {
		if (webPattern.getPattern() instanceof Association) {
			return ResultPatternGenerator.ASSOCIATION_GENERATOR.getHTML(
					session, webPattern);
		} else if (webPattern.getPattern() instanceof ExceptionalModelPattern) {
			return ResultPatternGenerator.EMM_GENERATOR.getHTML(session,
					webPattern);
		} else if (webPattern.getPattern() instanceof Outlier) {
			return ResultPatternGenerator.OUTLIER_GENERATOR.getHTML(session,
					webPattern);
		} else {
			return ResultPatternGenerator.DEFAULT_GENERATOR.getHTML(session,
					webPattern);
		}
	}

}
