package de.unibonn.creedo.webapp.patternviews;

import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.Pattern;

public abstract class AbstractResultPatternMapper extends
		GeneralPattenGenerator implements PatternHTMLGenerator {

	public enum AnnotationVisibility {
		INVISIBLE {
			@Override
			void fillHtmlStringBuffer(StringBuilder builder, WebPattern pattern) {
				;
			}
		},

		READONLY {
			@Override
			void fillHtmlStringBuffer(StringBuilder builder, WebPattern pattern) {
				if (pattern.getAnnotationText().isEmpty()) {
					return;
				}
				builder.append("<textarea readonly class='annotation' row ='1' >");
				builder.append(pattern.getAnnotationText());
				builder.append("</textarea>");
			}
		},

		EDITABLE {
			@Override
			void fillHtmlStringBuffer(StringBuilder builder, WebPattern pattern) {
				builder.append("<textarea class='annotation' row ='1' "
						+ "placeholder='Click to input annotation. Confirm with [ENTER]'>");
				builder.append(pattern.getAnnotationText());
				builder.append("</textarea>");
			}
		};

		abstract void fillHtmlStringBuffer(StringBuilder builder,
				WebPattern pattern);
	}

	private final AnnotationVisibility annotationVisibility;

	public AbstractResultPatternMapper(List<String> optionalActions,
			AnnotationVisibility annotationVisibility) {
		super(optionalActions);
		this.annotationVisibility = annotationVisibility;
	}

	@Override
	protected void fillHtmlStringBuffer(StringBuilder result,
			HttpSession session, WebPattern webPattern) {
        result.append("<div class=\"panel panel-default pattern "
                + getHTMLClass() + "\" id=\"" + webPattern.getId() + "\">");
		// header
		result.append(getTitleDiv(webPattern));

		result.append(getActionsDiv(webPattern));

		// panel body
		result.append("<div class=\"panel-body\">");

		result.append("<div class=\"left pull-left\">");

		result.append(getDescription(webPattern.getPattern()));
		result.append(getExplanation(webPattern.getPattern()));
		this.annotationVisibility.fillHtmlStringBuffer(result, webPattern);
		// result.append("<textarea class='annotation' row ='1' "
		// + "placeholder='Click to input annotation. Confirm with [ENTER]'>");
		// result.append(webPattern.getAnnotationText());
		// result.append("</textarea>");
        result.append("</div>");

		result.append(getIllustration(session, webPattern.getPattern()));

		result.append("<div class=\"clearfix\"></div>");

		// close panel-body and panel div tags
		result.append("</div></div>");
	}

	private String getTitleDiv(WebPattern webPattern) {
		return "<div class=\"panel-heading\"><div class=\"title pull-left\">"
                + getTitle(webPattern) + "</div>";
	}

	private String getDescription(Pattern pattern) {
		StringBuilder sb = new StringBuilder();
		sb.append("<div class='description'>");

		List<String> descriptorList = getDescriptionElements(pattern);

		if (!descriptorList.isEmpty()) {
			sb.append(descriptorList.get(0));

			for (int dscrpCount = 1; dscrpCount < descriptorList.size(); dscrpCount++) {
				sb.append("<br/>");
				sb.append(descriptorList.get(dscrpCount));
			}
		}
		sb.append("</div>");

		return sb.toString();
	}

	protected abstract String getTitle(WebPattern webPattern);

	protected abstract String getHTMLClass();

	protected abstract List<String> getDescriptionElements(Pattern pattern);

	protected abstract List<String> getExplanationElements(Pattern pattern);

	private String getExplanation(Pattern pattern) {
		StringBuilder sb = new StringBuilder();
		sb.append("<div class='explanation'>");
		for (String element : getExplanationElements(pattern)) {
			sb.append(element);
			sb.append("<br/>");
		}
		sb.append("</div>");
		return sb.toString();
	}

}
