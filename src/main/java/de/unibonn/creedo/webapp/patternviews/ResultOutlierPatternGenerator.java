package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.InterestingnessMeasure;
import de.unibonn.realkd.patterns.Pattern;

public class ResultOutlierPatternGenerator extends AbstractResultPatternMapper {

	public ResultOutlierPatternGenerator(List<String> optionalActions,
			AnnotationVisibility annotationVisibility) {
		super(optionalActions, annotationVisibility);
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		return "Outlier Pattern";
	}

	@Override
	protected String getHTMLClass() {
		return "generic-pattern";
	}

	@Override
	protected List<String> getDescriptionElements(Pattern pattern) {
		List<String> res = new ArrayList<>(pattern.getSupportSet().size());
		res.add("The rows");
		for (Integer index : pattern.getSupportSet()) {
			res.add(pattern.getDataArtifact().getObjectName(index));
		}
		if (!pattern.getAttributes().isEmpty()) {
			res.add("behave annormaly in terms of "
					+ getAttributesDescriptionSubstring(pattern) + ".");
		}
		return res;
	}

	private String getAttributesDescriptionSubstring(Pattern pattern) {
		StringBuffer resultBuffer = new StringBuffer();
		Iterator<Attribute> attributes = pattern.getAttributes().iterator();
		while (attributes.hasNext()) {
			resultBuffer.append("<strong>" + attributes.next().getName()
					+ "</strong>");
			if (attributes.hasNext()) {
				resultBuffer.append(", ");
			}
		}
		return resultBuffer.toString();
	}

	// @Override
	// protected List<String> getExplanationElements(Pattern pattern) {
	// ArrayList<String> result = new ArrayList<>();
	// result.add("Frequency: " + pattern.getFrequency());
	// return result;
	// }

	/*
	 * currently duplication with other resultMappers
	 */
	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		List<String> sb = new ArrayList<>();

		for (InterestingnessMeasure measure : pattern.getMeasures()) {
			sb.add(measure.getName()
					+ String.format(": %.4f", pattern.getValue(measure)));
		}

		return sb;
	}

}
