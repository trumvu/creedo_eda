package de.unibonn.creedo.webapp.service;


import java.util.List;

import de.unibonn.creedo.webapp.User;
import de.unibonn.creedo.webapp.dbaccess.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

/**
 * User: paveltokmakov
 * Date: 8/12/13
 */
public class MailService {

    private MailSender mailSender;

    private String senderEmail;
    
    @Autowired
    private UserDAO userDao;

    public void sendActivationEmail(User user, String baseURL) {
    	List<User> administrators = userDao.getAdministrators();

        for(User administrator: administrators) {
            sendActivationToAdmin(administrator, user, baseURL);
        }
    }

    private void sendActivationToAdmin(User administrator, User user, String baseURL) {
        SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom(senderEmail);
		message.setTo(administrator.getUsername());
		message.setSubject("New registration! User name: " + user.getUsername());
        String activationURL = baseURL + "/activate.htm?userId=" + user.getUsername();
		message.setText("New user has been registered: " + user.getUsername() + ". Click the following link to activate: "
                + activationURL);
		mailSender.send(message);
    }

    public void sendConfirmationEmail(User user) {
        SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom(senderEmail);
		message.setTo(user.getUsername());
		message.setSubject("Activation Confirmation");
		message.setText("Your account has been activated.");
		mailSender.send(message);
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

}
