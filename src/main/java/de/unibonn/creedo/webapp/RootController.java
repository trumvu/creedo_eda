package de.unibonn.creedo.webapp;

import java.nio.file.FileSystems;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ServerPaths;
import de.unibonn.creedo.clienttools.database.InitDatabaseUtil;
import de.unibonn.creedo.common.BCrypt;
import de.unibonn.creedo.common.ConfigurationProperties;
import de.unibonn.creedo.utils.SimpleCopyFileVisitor;
import de.unibonn.creedo.webapp.dbaccess.UserDAO;
import de.unibonn.realkd.common.logger.LogChannel;
import de.unibonn.realkd.common.logger.LogMessageType;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

/**
 * This controller contains utility services like e.g. a service that can be
 * called in order to recreate the database.
 */

@Controller
@RequestMapping(value = "/root")
public class RootController implements ServletContextAware {
	private ServletContext context;

	@Autowired
	private HttpSession session;

	private UserDAO userDAO = new UserDAO();

	private LogChannel log = LogChannel.DEFAULT;

	@RequestMapping(value = "/initDatabase.htm", method = RequestMethod.GET)
	public ModelAndView initialize(
			@RequestParam(value = "setuppw") String setupPassword,
			@RequestParam(value = "adminmail") String adminEmail,
			@RequestParam(value = "adminpass") String adminPassword,
			@RequestParam(value = "increment", required = false, defaultValue = "false") Boolean onlyIncrement)
			throws Exception {

		ModelAndView mav = new ModelAndView("util/databaseRecreation");

		ConfigurationProperties config = ConfigurationProperties.get();

		try {
			if (!setupPassword.equals(config.CREEDO_SETUP_PASS)) {
				throw new IllegalArgumentException("Root password invalid.");
			}

			String directory = context.getRealPath("database/") + "/"
					+ config.CREEDO_ACTIVE_PLUGIN;

			InitDatabaseUtil.initialize(directory, config.CREEDO_DB_HOST,
					config.CREEDO_DB_PORT, config.CREEDO_DB_SCHEMA,
					config.CREEDO_DB_USER, config.CREEDO_DB_PASS, adminEmail,
					adminPassword, !onlyIncrement);
			log.log("Successfully applied database scripts",
					LogMessageType.DEBUG_MESSAGE);

			Files.walkFileTree(ServerPaths.ABS_PATH_TO_SELECTED_PLUGIN_DIR,
					new SimpleCopyFileVisitor(
							ServerPaths.ABS_PATH_TO_CONFIGURED_RESSOURCE_DIR,
							StandardCopyOption.REPLACE_EXISTING,
							StandardCopyOption.COPY_ATTRIBUTES));

			mav.addObject("status", "success");
		} catch (Exception e) {
			log.log(e.getMessage(), LogMessageType.DEBUG_MESSAGE);
			mav.addObject("status", "failed: " + e.getMessage());
		}

		return mav;
	}

	@RequestMapping(value = "/changePassword.htm", method = RequestMethod.GET)
	public ModelAndView initialize(@RequestParam(value = "oldpw") String oldPw,
			@RequestParam(value = "newpw") String newPw) {

		CreedoSession creedoSession = Creedo.getCreedoSession(session);

		User currentUser = creedoSession.getUser();

		ModelAndView mav = new ModelAndView("util/userUpdate");

		if (BCrypt.checkpw(oldPw, currentUser.getPassword())) {
			String newHashedPw = BCrypt.hashpw(newPw, BCrypt.gensalt(12));
			currentUser.setPassword(newHashedPw);
			userDAO.updateUser(currentUser);
			mav.addObject("status", "success");
		} else {
			mav.addObject("status", "failed. Old password doesn't match.");
		}

		return mav;
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}
}
