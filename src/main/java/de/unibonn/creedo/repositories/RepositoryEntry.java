package de.unibonn.creedo.repositories;

public interface RepositoryEntry<T> {

	public String getId();
		
	public T getContent();
	
}
