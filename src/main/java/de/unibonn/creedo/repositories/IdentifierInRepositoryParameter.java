package de.unibonn.creedo.repositories;

import java.util.List;

//import com.google.common.base.Predicate;
//import com.google.common.collect.Iterables;
//import com.google.common.collect.Lists;

import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;

/**
 * Range enumarable parameter that can take on all values that match identifiers
 * in a given repository.
 * 
 * @see {@link RangeEnumerableParameter}, {@link Repository}
 * 
 * @author mboley
 *
 */
public class IdentifierInRepositoryParameter<T> extends
		DefaultRangeEnumerableParameter<String> {

	public IdentifierInRepositoryParameter(final String name,
			final String description, final Repository<T> repository,
			final Predicate<RepositoryEntry<T>> filterPredicate) {
		super(name, description, String.class, new RangeComputer<String>() {
			@Override
			public List<String> computeRange() {
				return repository.getAll(filterPredicate).stream()
						.map(entry -> entry.getId())
						.collect(Collectors.toList());
				// Iterable<RepositoryEntry<T>> matching = repository
				// .getAll(filterPredicate);
				// return Lists.newArrayList(Iterables.transform(matching,
				// Repositories.ENTRY_TO_ID_FUNCTION));
			}
		});
	}

	public IdentifierInRepositoryParameter(final String name,
			final String description, final Repository<T> repository) {
		this(name, description, repository, entry -> true);

		// this(name, description, repository,
		// Repositories.getEntryPredicate());
		// super(name, description, type, new RangeComputer<String>() {
		// @Override
		// public List<String> computeRange() {
		// return repository.getAllIds();
		// }
		// });
	}

}
