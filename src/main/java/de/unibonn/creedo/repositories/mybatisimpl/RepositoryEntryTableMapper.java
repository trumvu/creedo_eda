package de.unibonn.creedo.repositories.mybatisimpl;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface RepositoryEntryTableMapper {

	public void insertEntryRow(@Param("tableName") String entryTableName,
			@Param("id") String id,
			@Param("contentClassName") String contentClassName);

	public EntryTableRow getEntryRow(@Param("tableName") String entryTableName,
			@Param("id") String id);

	public List<EntryTableRow> getAllEntryRows(
			@Param("tableName") String entryTableName);

	public List<ParameterTableRow> getEntryParameterRows(
			@Param("tableName") String parameterTableName,
			@Param("entryId") String entryId);

	public void deleteEntryContentParameterRows(
			@Param("tableName") String parameterTableName,
			@Param("entryId") String entryId);

	public void deleteEntryRow(@Param("tableName") String entriesTableName,
			@Param("id") String id);

	public void insertEntryContentParameterRow(
			@Param("tableName") String parameterTableName,
			@Param("entryId") String entryId,
			@Param("parameterName") String name,
			@Param("parameterValue") String string);

}
