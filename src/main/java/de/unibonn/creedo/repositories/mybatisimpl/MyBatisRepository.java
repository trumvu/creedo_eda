package de.unibonn.creedo.repositories.mybatisimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import de.unibonn.creedo.common.ConnectionFactory;
import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

public class MyBatisRepository<T> implements Repository<T> {

	private String name;

	private String entriesTableName;

	private String contentParametersTableName;

	private SqlSessionFactory sqlSessionFactory;

	public MyBatisRepository(String name, String entriesTableName,
			String contentParametersTableName) {
		this.name = name;
		this.sqlSessionFactory = ConnectionFactory.get().getSession();
		this.entriesTableName = entriesTableName;
		this.contentParametersTableName = contentParametersTableName;
	}

	@Override
	public void add(String id, T content) {
		MyBatisRepositoryEntry<T> entry = new MyBatisRepositoryEntry<T>(id,
				content);
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session
				.getMapper(RepositoryEntryTableMapper.class);
		mapper.insertEntryRow(entriesTableName, entry.getId(), entry
				.getContent().getClass().getCanonicalName());
		if (content instanceof ParameterContainer) {
			for (Parameter<?> param : ((ParameterContainer) content)
					.getTopLevelParameters()) {
				mapper.insertEntryContentParameterRow(
						contentParametersTableName, entry.getId(),
						param.getName(), param.getCurrentValue().toString());
			}
		}
		session.commit();
		session.close();
	}

	@Override
	public void delete(String id) {
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session
				.getMapper(RepositoryEntryTableMapper.class);

		mapper.deleteEntryContentParameterRows(contentParametersTableName, id);
		mapper.deleteEntryRow(entriesTableName, id);

		session.commit();
		session.close();
	}

	@Override
	public void update(RepositoryEntry<T> entry) {
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session
				.getMapper(RepositoryEntryTableMapper.class);

		T content = entry.getContent();

		if (content instanceof ParameterContainer) {
			mapper.deleteEntryContentParameterRows(contentParametersTableName,
					entry.getId());
			for (Parameter<?> param : ((ParameterContainer) content)
					.getAllParameters()) {
				mapper.insertEntryContentParameterRow(
						contentParametersTableName, entry.getId(),
						param.getName(), param.getCurrentValue().toString());
			}
		}
		session.commit();
		session.close();
	}

	@Override
	public RepositoryEntry<T> get(String id) {
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session
				.getMapper(RepositoryEntryTableMapper.class);
		EntryTableRow entryRow = mapper.getEntryRow(this.entriesTableName, id);
		List<ParameterTableRow> parameterRows = mapper.getEntryParameterRows(
				this.contentParametersTableName, id);
		session.close();

		T content = getContentInstance(entryRow, parameterRows);

		return new MyBatisRepositoryEntry<T>(entryRow.getId(), content);
	}

	private T getContentInstance(EntryTableRow entryRow,
			List<ParameterTableRow> parameterRows) {
		Class<?> contentClass = null;
		try {
			contentClass = Class.forName(entryRow.getContentClassName());
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException("Could not find content class: "
					+ entryRow.getContentClassName());
		}
		T content = null;
		try {
			content = (T) contentClass.newInstance();
		} catch (InstantiationException e) {
			throw new IllegalStateException("Could not instantiate class: "
					+ entryRow.getContentClassName());
		} catch (IllegalAccessException e) {
			throw new IllegalStateException("Could not access constructor of: "
					+ entryRow.getContentClassName());
		}
		if (content instanceof ParameterContainer) {
			for (ParameterTableRow row : parameterRows) {
				((ParameterContainer) content).findParameterByName(
						row.getParameterName()).setByString(
						row.getParameterValue());
			}
		}
		return content;
	}

	@Override
	public List<RepositoryEntry<T>> getAll() {
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session
				.getMapper(RepositoryEntryTableMapper.class);
		List<EntryTableRow> entryRows = mapper
				.getAllEntryRows(this.entriesTableName);

		List<RepositoryEntry<T>> result = new ArrayList<>();
		for (EntryTableRow row : entryRows) {
			List<ParameterTableRow> parameterRows = mapper
					.getEntryParameterRows(this.contentParametersTableName,
							row.getId());
			result.add(new MyBatisRepositoryEntry<T>(row.getId(),
					getContentInstance(row, parameterRows)));
		}

		session.close();
		return result;
	}

	@Override
	public List<String> getAllIds() {
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session
				.getMapper(RepositoryEntryTableMapper.class);
		List<EntryTableRow> entryRows = mapper
				.getAllEntryRows(this.entriesTableName);
		session.close();

		List<String> result = new ArrayList<>();
		for (EntryTableRow row : entryRows) {
			result.add(row.getId());
		}

		return result;
	}

	@Override
	public String getName() {
		return name;
	}

}
