package de.unibonn.creedo.ui;

import java.io.InputStream;
import java.util.Scanner;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.dbaccess.DataTableDAO;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author bjacobs
 */

public class DatabaseUploadPage implements Page {
	private boolean databaseAddTried = false;
	private boolean databaseAddSuccessful = false;
	private String errorMessage;

	@Override
	public String getTitle() {
		return "Upload your own database";
	}

	@Override
	public String getReferenceName() {
		return "Upload database";
	}

	@Override
	public String getViewImport() {
		return "uploadDatabasePage.jsp";
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();

		if (databaseAddTried) {
			if (databaseAddSuccessful) {
				model.addAttribute("statusAddDatabase", "success");
			} else {
				model.addAttribute("statusAddDatabase", "failed: " + errorMessage);
			}
		}

		databaseAddTried = false;

		return model;
	}

	public void addDatabase(MultipartFile attributesFile, MultipartFile dataFile,
			MultipartFile attributeGroupsFile, String dataDelimiter, String missingSymbol,
			String databaseName, String databaseDescription)
			throws Exception {

		databaseAddTried = true;

		if ((attributesFile.isEmpty() || dataFile.isEmpty())) {
			databaseAddSuccessful = false;
			errorMessage = "No data files were provided.";
		} else {
			// using files
			String dataFileContent = convertStreamToString(dataFile
					.getInputStream());
			String attributeFileContent = convertStreamToString(attributesFile
					.getInputStream());
			String attributeGroupsFileContent = convertStreamToString(attributeGroupsFile
					.getInputStream());

			DataTableDAO.INSTANCE.storeDatabase(databaseName, databaseDescription,
					dataDelimiter, missingSymbol, dataFileContent, attributeFileContent,
					attributeGroupsFileContent);

			databaseAddSuccessful = true;
		}
	}

	private static String convertStreamToString(InputStream is) {
		Scanner s = new Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

}
