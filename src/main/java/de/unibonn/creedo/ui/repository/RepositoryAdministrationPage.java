package de.unibonn.creedo.ui.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister;
import de.unibonn.creedo.webapp.utils.JsonTransferParameterContainer;
import de.unibonn.creedo.webapp.utils.ParameterToJsonConverter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

/**
 * Page for adding, deleting, and modifying entries of a repository.
 * 
 * @author bjacobs, mboley
 * 
 */
public class RepositoryAdministrationPage<T> implements Page, ActionProvider,
		UiComponent {

	private Map<Integer, Integer> actionIdToParameterIndexMap;
	@SuppressWarnings("rawtypes")
	private final int id;
	private Repository<T> repository;
	private List<String> typeAliases;
	private final RepositoryEntryDeletionAction deletionAction;
	private final RepositoryEntryAdditionAction<T> additionAction;
	private final UiRegister uiRegister;
	private final Map<Integer, Action> updateParameterActions;

	public RepositoryAdministrationPage(int componentId,
			Repository<T> repository, List<String> typeAliases,
			RepositoryEntryDeletionAction deletionAction,
			RepositoryEntryAdditionAction<T> additionAction,
			UiRegister uiRegister) {
		id = componentId;
		actionIdToParameterIndexMap = new HashMap<>();
		this.repository = repository; //
		this.typeAliases = typeAliases;
		this.deletionAction = deletionAction;
		this.additionAction = additionAction;
		this.uiRegister = uiRegister;
		this.updateParameterActions = new HashMap<>();
	}

	@Override
	public String getTitle() {
		return repository.getName() + " management";
	}

	@Override
	public String getReferenceName() {
		return repository.getName();
	}

	@Override
	public String getViewImport() {
		return "repositoryAdministrationPage.jsp";
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return getViewImport();
	}

	public Model getModel() {
		// first code block prepares all update actions and the model for their
		// visual representation (transfer containers)
		updateParameterActions.clear();

		List<EntryTransferContainer> entryTransferContainers = new ArrayList<>();

		for (String entryId : repository.getAllIds()) {

			RepositoryEntry<T> entry = repository.get(entryId);

			// TODO: The code relies here implicitly on the fact that the next
			// two lists will have the same number of
			// entries. This seems very fragile. Generally compiling the full
			// list of parameters (including nested) should have framework
			// support. Apparently there is a lot of duplication already for
			// compiling this list in various clients.
			//
			// Also this assumes that all entries have parameter container
			// contents, which is not necessarily the case!!
			T content = entry.getContent();
			@SuppressWarnings("rawtypes")
			List<Parameter> allParameters = ((ParameterContainer) content)
					.getAllParameters(); // getParametersRecursively(parameters);
			List<JsonTransferParameterContainer> paramTransferContainers = ParameterToJsonConverter
					.convertRecursively(((ParameterContainer) content)
							.getTopLevelParameters());

			for (int i = 0; i < paramTransferContainers.size(); i++) {
				Parameter<?> parameter = allParameters.get(i);

				RepositoryParameterUpdateAction<T> updateAction = new RepositoryParameterUpdateAction<T>(
						uiRegister.getNextId(), repository, entryId, parameter);

				updateParameterActions.put(updateAction.getId(), updateAction);

				paramTransferContainers.get(i)
						.setActionId(updateAction.getId());

			}

			String name = content.getClass().getSimpleName();

			entryTransferContainers.add(new EntryTransferContainer(entryId,
					name, paramTransferContainers));
		}

		BindingAwareModelMap map = new BindingAwareModelMap();
		map.put("entries", entryTransferContainers);
		map.put("componentId", id);
		map.put("entryDeletionAction", deletionAction);
		map.put("entryAdditionAction", additionAction);
		map.put("additionTypes", typeAliases);
		return map;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Collections.emptyList();
	}

	@Override
	public Collection<Integer> getActionIds() {
		return actionIdToParameterIndexMap.keySet();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		if (id == deletionAction.getId()) {
			return deletionAction.activate(params);
		}

		if (id == additionAction.getId()) {
			return additionAction.activate(params);
		}

		return updateParameterActions.get(id).activate(params);
	}

	@Override
	public boolean isActionAvailable(int id) {
		return false;
	}

	public class EntryTransferContainer {
		private final String id;
		private final String name;
		private final List<JsonTransferParameterContainer> parameters;

		public EntryTransferContainer(String id, String name,
				List<JsonTransferParameterContainer> parameters) {
			this.id = id;
			this.name = name;
			this.parameters = parameters;
		}

		public String getName() {
			return name;
		}

		public String getId() {
			return id;
		}

		public List<JsonTransferParameterContainer> getParameters() {
			return parameters;
		}
	}
}
