package de.unibonn.creedo.ui.repository;

import static com.google.common.base.Preconditions.checkArgument;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.ui.core.Action;

public class RepositoryEntryDeletionAction implements Action {

	private final int id;

	private final Repository<?> repository;

	public RepositoryEntryDeletionAction(int id, Repository<?> repository) {
		this.id = id;
		this.repository = repository;
	}

	@Override
	public String getReferenceName() {
		return "Delete";
	}

	/**
	 * Deletes an entry from the repository that this action is linked to.
	 * 
	 * @param param
	 *            expects exactly one parameter corresponding to the identifier
	 *            of the repository entry that is to be deleted
	 */
	@Override
	public ResponseEntity<String> activate(String... params) {
		checkArgument(params.length == 1, "Expected one parameter for delete.");
		repository.delete(params[0]);
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return ClientWindowEffect.REFRESH;
	}

	@Override
	public int getId() {
		return id;
	}

}
