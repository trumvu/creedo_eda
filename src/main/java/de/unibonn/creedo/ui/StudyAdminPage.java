package de.unibonn.creedo.ui;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.Page;

public class StudyAdminPage implements Page {

	@Override
	public String getTitle() {
		return "Study Administration";
	}

	@Override
	public String getReferenceName() {
		return "Studies";
	}

	@Override
	public String getViewImport() {
		return "studyAdminPage.jsp";
	}

	@Override
	public Model getModel() {
        // Get all studies OK
        // Sort by name
		Model model=new BindingAwareModelMap();
//		StudyDAO studyDAO=new StudyDAO();
//        List<Study> studyList = studyDAO.getAllOpenStudies();
//        List<Pair<String, Integer>> result = new ArrayList<>();
//
//        for (Study study : studyList) {
//            result.add(new Pair<>(study.getName(), study.getStudyId()));
//        }
//        model.addAttribute("dbaccess/studies", result);
        return model;
	}


}
