package de.unibonn.creedo.ui;

import static de.unibonn.realkd.common.logger.LogChannel.DEFAULT;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import de.unibonn.creedo.common.ConfigurationProperties;
import de.unibonn.creedo.common.Pair;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.dashboard.AnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.mining.DataTableProvider;
import de.unibonn.creedo.webapp.dashboard.mining.MiningDashboardModelBuilder;
import de.unibonn.creedo.webapp.dashboard.mining.MiningDashboardModelBuilderImplementation;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.creedo.webapp.dbaccess.DataTableDAO;
import de.unibonn.creedo.webapp.utils.serialization.AlgorithmFactory;
import de.unibonn.realkd.algorithms.MiningAlgorithmFactory;
import de.unibonn.realkd.common.logger.LogChannel;
import de.unibonn.realkd.common.logger.LogMessageType;
import de.unibonn.realkd.common.parameter.Parameter;

/**
 * Model class for all custom-dashboard-creation related logic.
 *
 * @author bkang, bjacobs
 * 
 */
public class CustomDashboardCreationPage implements Page {

	private MiningSystemBuilder miningSystemBuilder;
	private static List<MiningSystemBuilder> miningSystemBuilders = new ArrayList<>();

	static {
		// Load information from the database which MiningSystemBuilders to use
		// DashboardDAO dashboardDAO = new DashboardDAO();
		List<String> builderClassNames = ConfigurationProperties.get().MINING_SYSTEM_BUILDER_CLASSES;
		// dashboardDAO.getCustomDashboardBuilderClassNames();

		// Then instantiate the mentioned MiningSystemBuilders
		for (String className : builderClassNames) {
			try {
				Object instance = Class.forName(className).newInstance();
				miningSystemBuilders.add((MiningSystemBuilder) instance);
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException e) {
				DEFAULT.log(
						"Could not find or instantiate MiningSystemBuilder by name: "
								+ className + " because " + e.getMessage(),
						LogMessageType.INTRA_COMPONENT_MESSAGE);
				e.printStackTrace();
			}
		}
	}

	private static LogChannel UI = LogChannel.UI;

	public CustomDashboardCreationPage() {
		initModel();
	}

	private void initModel() {
		this.miningSystemBuilder = miningSystemBuilders.get(0);
	}

	public ModelAndView getView() {
		ModelAndView mav = new ModelAndView("dashboard/customDashboard");

		// Load available databases information
		List<Pair<String, Integer>> dataTables = loadDataTableInformation();
		mav.addObject("dataTables", dataTables);

		// Load available system variants information
		List<Pair<String, Integer>> systemVariants = loadSystemVariantsInformation();
		mav.addObject("systemVariants", systemVariants);

		// Load available Ranker Factories for one click mining dash board
		// builder
		/*
		 * List<Pair<String, String>> rankerFactories =
		 * loadRankerFactoryInformation(); mav.addObject("rankerFactories",
		 * rankerFactories);
		 */

		// load available algorithms information
		List<Pair<String, Boolean>> miningAlgorithms = loadAlgorithmInformation();
		mav.addObject("miningAlgorithms", miningAlgorithms);

		return mav;
	}

	private List<Pair<String, Integer>> loadSystemVariantsInformation() {
		List<Pair<String, Integer>> result = new ArrayList<>();

		for (int i = 0; i < miningSystemBuilders.size(); i++) {
			result.add(new Pair<>(((Object) miningSystemBuilders.get(i))
					.toString(), i));
		}

		return result;
	}

	private List<Pair<String, Integer>> loadDataTableInformation() {
		DataTableDAO dataTableDAO = new DataTableDAO();

		List<Integer> dataTableIds = dataTableDAO.getDatatableIds();
		List<Pair<String, Integer>> result = new ArrayList<>();
		for (Integer i : dataTableIds) {
			String name = dataTableDAO.getNameById(i);
			result.add(new Pair<>(name, i));
		}
		return result;
	}

	// TODO to be removed when algorithms become parameter of builder
	private List<Pair<String, Boolean>> loadAlgorithmInformation() {
		List<Pair<String, Boolean>> result = new ArrayList<>();

		for (MiningAlgorithmFactory algorithm : AlgorithmFactory.values()) {// getConfiguredAlgorithms())
																			// {
			result.add(new Pair<>(algorithm.toString(), false));
		}
		return result;
	}

	public AnalyticsDashboardBuilder createCustomDashboardBuilder(
			MultipartFile attributesFile, MultipartFile dataFile,
			MultipartFile attributeGroupsFile, Integer dataTableId,
			String dataDelimiter, String missingSymbol,
			// String rankerFactoryName,
			Integer[] userSelectedAlgorithmIds, Integer selectedTabIndex)
			throws IOException {

		// Identify system
		DataTableProvider dataTableProvider;
		if (selectedTabIndex == 0) {
			// use dataTableId
			dataTableProvider = new DataTableProvider.DbBasedDataTableProvider(
					dataTableId);
		} else {
			if ((attributesFile.isEmpty() || dataFile.isEmpty())) {
				String msg = "No data files were provided.";
				UI.log(msg, LogMessageType.DEBUG_MESSAGE);
				throw new IllegalArgumentException(msg);
			}

			// using files
			String dataFileContent = convertStreamToString(dataFile
					.getInputStream());
			String attributeFileContent = convertStreamToString(attributesFile
					.getInputStream());
			String attributeGroupsFileContent = convertStreamToString(attributeGroupsFile
					.getInputStream());
			dataTableProvider = new DataTableProvider.FileBasedDataTableProvider(
					dataFileContent, attributeFileContent,
					attributeGroupsFileContent, dataDelimiter, missingSymbol);
		}

		List<AlgorithmFactory> algorithms = compileAlgorithmList(userSelectedAlgorithmIds);

		MiningDashboardModelBuilder builder;

		// Set values in builder TODO to be removed by using parameters
		miningSystemBuilder.setAlgorithmFactories(algorithms);

		builder = new MiningDashboardModelBuilderImplementation(
				dataTableProvider, miningSystemBuilder);

		return builder;
	}

	private List<AlgorithmFactory> compileAlgorithmList(Integer[] algorithmIds) {
		List<AlgorithmFactory> algorithms = new ArrayList<>();

		for (Integer index : algorithmIds) {
			algorithms.add(AlgorithmFactory.values()[index]);
		}
		return algorithms;
	}

	private static String convertStreamToString(InputStream is) {
		Scanner s = new Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	public void setSystemVariant(Integer systemVariantId) {
		if (systemVariantId == null || systemVariantId < 0
				|| systemVariantId > miningSystemBuilders.size()) {
			throw new IllegalArgumentException(
					"Invalid value for system variant id: " + systemVariantId);
		}

		this.miningSystemBuilder = miningSystemBuilders.get(systemVariantId);
	}

	public List<Parameter> getParameters() {
		return miningSystemBuilder.getTopLevelParameters();
	}

	public void setParameter(String parameterName, String parameterValue) {
		try {
			miningSystemBuilder.findParameterByName(parameterName).setByString(
					parameterValue);
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"Could not set parameter \"" + parameterName
							+ "\" to value \"" + parameterValue + "\"!", e);
		}

	}

	@Override
	public String getTitle() {
		return "Create custom Analytics Dashboard";
	}

	@Override
	public String getReferenceName() {
		return "Custom Dashboard";
	}

	@Override
	public String getViewImport() {
		return "dashboardCreationPage.jsp";
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();
		List<Pair<String, Integer>> dataTables = loadDataTableInformation();
		model.addAttribute("dataTables", dataTables);

		// Load available system variants information
		List<Pair<String, Integer>> systemVariants = loadSystemVariantsInformation();
		model.addAttribute("systemVariants", systemVariants);

		// Load available Ranker Factories for one click mining dash board
		// builder
		/*
		 * List<Pair<String, String>> rankerFactories =
		 * loadRankerFactoryInformation(); mav.addObject("rankerFactories",
		 * rankerFactories);
		 */

		// load available algorithms information
		List<Pair<String, Boolean>> miningAlgorithms = loadAlgorithmInformation();
		model.addAttribute("miningAlgorithms", miningAlgorithms);
		return model;
	}

}
