//package de.unibonn.creedo.ui.standard;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import de.unibonn.creedo.Repositories;
//import de.unibonn.creedo.RuntimeBuilder;
//import de.unibonn.creedo.repositories.RepositoryEntry;
//import de.unibonn.creedo.ui.core.Action;
//import de.unibonn.creedo.ui.core.LoadPageAction;
//import de.unibonn.creedo.ui.core.Page;
//import de.unibonn.creedo.ui.core.PageContainer;
//import de.unibonn.creedo.webapp.CreedoSession;
//
//public class ContentIdsToActionsConverter {
//
//	private PageContainer pageContainer;
//	private CreedoSession creedoSession;
//
//	public ContentIdsToActionsConverter(PageContainer pageContainer,
//			CreedoSession creedoSession) {
//		this.pageContainer = pageContainer;
//		this.creedoSession = creedoSession;
//	}
//
//	public List<Action> convert(List<String> contentIds) {
//		List<Action> result = new ArrayList<>();
//		for (String id : contentIds) {
//			RepositoryEntry<RuntimeBuilder<?, ?>> entry = Repositories.PAGE_REPOSITORY
//					.get(id);
//			if (entry.getType().equals("Page")) {
//				Page page = ((RuntimeBuilder<Page, CreedoSession>) entry
//						.getContent()).build(creedoSession);
//
//				result.add(new LoadPageAction(id, page, pageContainer));
//			}
//		}
//		return result;
//	}
//
//}
