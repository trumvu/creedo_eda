package de.unibonn.creedo.ui.standard;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.common.ConfigurationProperties;
import de.unibonn.creedo.ui.core.ActionLink;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.realkd.RealKD;

public class Footer implements ActionProvider, UiComponent {

	private final Map<Integer, ActionLink> idToActionLink;

	private final int id;

	private final Model model;

	public Footer(int id, List<ActionLink> actionLinks) {
		this.id = id;
		this.idToActionLink = new HashMap<Integer, ActionLink>();
		for (ActionLink link : actionLinks) {
			if (idToActionLink.containsKey(link.getId())) {
				throw new IllegalArgumentException("Action with id "
						+ link.getId() + " already present in footer.");
			}
			idToActionLink.put(link.getId(), link);
		}
		model = new BindingAwareModelMap();
		model.addAttribute("footerLinks", actionLinks);
		model.addAttribute(
				"footerStaticText",
				ConfigurationProperties.get().COPYRIGHT_NOTICE + ". Running "
						+ Creedo.getName() + " " + Creedo.getVersion()
						+ " with " + RealKD.getName() + " "
						+ RealKD.getVersion());
	}

	@Override
	public Collection<Integer> getActionIds() {
		return idToActionLink.keySet();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		return idToActionLink.get(id).activate();
	}

	@Override
	public boolean isActionAvailable(int id) {
		return idToActionLink.containsKey(id);
	}

	public Model getModel() {
		return model;
	}

	@Override
	public String getView() {
		return "footer.jsp";
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}

}
