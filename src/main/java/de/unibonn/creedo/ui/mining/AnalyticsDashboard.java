package de.unibonn.creedo.ui.mining;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystem;
import de.unibonn.creedo.webapp.viewmodels.DataTableViewModel;
import de.unibonn.creedo.webapp.viewmodels.MetaDataTableModel;
import de.unibonn.creedo.webapp.viewmodels.PointCloudViewModel;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;

public class AnalyticsDashboard implements UiComponent {

	private final HttpSession httpSession;

	private final MiningSystem miningSystem;
	private final DataViewContainer dataViewContainer;
	private final int id;

	public AnalyticsDashboard(int id, HttpSession httpSession,
			DataWorkspace dataWorkspace, MiningSystem miningSystem) {
		this.httpSession = httpSession;
		// this.dataWorkspace = dataWorkspace;
		this.miningSystem = miningSystem;
		this.id = id;
		this.dataViewContainer = new DataViewContainer(Creedo
				.getCreedoSession(httpSession).getUiRegister().getNextId(),
				httpSession, dataWorkspace);
	}

	@Override
	public String getView() {
		return "analyticsDashboard.jsp";
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();
		String datasetName = getDataTable().getName();
		model.addAttribute("jsFileName", getMiningSystem()
				.getMineButtonStrategy().getClientJSFileName());

		model.addAllAttributes(dataViewContainer.getModel().asMap());

		model.addAttribute("title", datasetName);
//		model.addAttribute("metaDataModel", getMetaDataModel());
		model.addAttribute("analyticsDashboardId", getId());
		return model;
	}

	public MiningSystem getMiningSystem() {
		return miningSystem;
	}

	public DataTable getDataTable() {
		return dataViewContainer.getDataTable();
	}

	public PropositionalLogic getPropositionalLogic() {
		return dataViewContainer.getPropositionalLogic();
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList((UiComponent) dataViewContainer);
	}

}
