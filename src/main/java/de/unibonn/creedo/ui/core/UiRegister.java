package de.unibonn.creedo.ui.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.ui.standard.NavbarFooterModalGroup;
import de.unibonn.creedo.webapp.dashboard.AnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;

/**
 * Stores ui components them for later reference via id.
 * 
 * @author mboley
 *
 */
public class UiRegister {

	private int nextId;

	private final HttpSession httpSession;

	private final Map<Integer, UiComponent> idToUiComponents;

	public UiRegister(HttpSession httpSession) {
		this.nextId = 0;
		this.idToUiComponents = new HashMap<>();
		this.httpSession = httpSession;
	}

	public DefaultPageFrame createDefaultFrame(PageContainer pageContainer,
			List<Action> navbarActions, List<Action> footerActions) {
		int id = getNextId();

		NavbarFooterModalGroup modalGroup = new NavbarFooterModalGroup(this,
				navbarActions, footerActions);

		DefaultPageFrame result = new DefaultPageFrame(id, pageContainer,
				modalGroup.getNavbar(), modalGroup.getFooter());

		// init frame to show first navbar page
		if (!navbarActions.isEmpty()) {
			result.performAction(navbarActions.get(0).getId());
		}

		registerUiComponent(result);
		return result;
	}

	public synchronized int getNextId() {
		return nextId++;
	}

	public DashboardModel createFrameFromBuilder(
			AnalyticsDashboardBuilder builder) {
		int id = getNextId();
		DashboardModel frame = builder.build(id, httpSession);
		registerUiComponent(frame);
		return frame;
	}

	public void registerUiComponent(UiComponent component) {
		if (idToUiComponents.containsKey(component.getId())) {
			System.out.println("WARNING: Component with same id "
					+ component.getId() + " is already registered.");
			return;
		}
		this.idToUiComponents.put(component.getId(), component);
		for (UiComponent childComponent : component.getComponents()) {
			registerUiComponent(childComponent);
		}
	}

	public UiComponent retrieveUiComponent(int id) {
		UiComponent result = idToUiComponents.get(id);
		if (result == null) {
			throw new IllegalArgumentException("No ui component with id " + id);
		}
		return result;
	}

}
