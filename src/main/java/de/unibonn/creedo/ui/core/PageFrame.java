package de.unibonn.creedo.ui.core;

public interface PageFrame extends Frame {
	
	public void loadPage(Page page);

}
