package de.unibonn.creedo.ui.core;

import org.springframework.http.ResponseEntity;

/**
 * Interface for ui actions that can be triggered by the user.
 * 
 * @author mboley
 *
 */
public interface Action {

	public enum ClientWindowEffect {
		REFRESH, REDIRECT, POPUP, CLOSE;
	}

	/**
	 * 
	 * @return name that can be used within the ui to refer to the action
	 * 
	 */
	public String getReferenceName();

	/**
	 * Performs the action changing the state of the ui.
	 * 
	 */
	public ResponseEntity<String> activate(String... params);

	public ClientWindowEffect getEffect();

	/**
	 * 
	 * @return id string that is unique within the reference space (that is
	 *         unique among actions in frame or page, respectively), in which
	 *         controller will try to resolve action
	 * 
	 */
	public int getId();

}
