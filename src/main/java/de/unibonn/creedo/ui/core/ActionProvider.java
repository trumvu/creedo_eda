package de.unibonn.creedo.ui.core;

import java.util.Collection;

import org.springframework.http.ResponseEntity;

public interface ActionProvider {
	
	public Collection<Integer> getActionIds();
	
	public ResponseEntity<String> performAction(int id, String... params);
	
	public boolean isActionAvailable(int id);

}
