package de.unibonn.creedo.ui.signup;

import static com.google.common.base.Preconditions.checkArgument;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.common.BCrypt;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.webapp.User;
import de.unibonn.creedo.webapp.dbaccess.UserDAO;

public class RequestAccountAction implements Action {

	private static final String EMAIL_ALREADY_IN_USE_MSG = "This Email address is already in use.";

	private Action successAction;

	private final int id;

	public RequestAccountAction(int id) {
		this.id = id;
	}

	@Override
	public String getReferenceName() {
		return "Register";
	}

	public void setOnSuccessAction(Action successAction) {
		this.successAction = successAction;
	}

	/**
	 * Registers new account in db if email address not already present.
	 * 
	 * @param params
	 *            must have exactly two elements; first element is email, second
	 *            is password (will be stored encrypted)
	 */
	@Override
	public ResponseEntity<String> activate(String... params) {
		checkArgument(params != null, "params must be not null");
		checkArgument(params.length == 2,
				"params must contain exactly two elements");

		String username = params[0];
		String password = params[1];

		UserDAO userDAO = new UserDAO();

		if (userDAO.isUserExists(username)) {
			return new ResponseEntity<String>(EMAIL_ALREADY_IN_USE_MSG,
					HttpStatus.EXPECTATION_FAILED);
		}

		Date dt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(dt);

		User user = new User();
		user.setUsername(username);
		user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt(12)));
		user.setRegistrationDate(currentTime);

		userDAO.saveUser(user);

		// mailService.sendActivationEmail(
		// user,
		// "http://" + request.getServerName() + ":"
		// + request.getServerPort() + request.getContextPath());

		if (this.successAction != null) {
			return successAction.activate();
		} else {
			return new ResponseEntity<String>(HttpStatus.OK);
		}
	}

	@Override
	public ClientWindowEffect getEffect() {
		return ClientWindowEffect.REFRESH;
	}

	@Override
	public int getId() {
		return id;
	}

}
