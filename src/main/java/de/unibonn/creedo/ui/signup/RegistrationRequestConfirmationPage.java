package de.unibonn.creedo.ui.signup;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.Page;

public class RegistrationRequestConfirmationPage implements Page {

	@Override
	public String getTitle() {
		return "Registration Request Confirmation";
	}

	@Override
	public String getReferenceName() {
		return "Confirmation";
	}

	@Override
	public String getViewImport() {
		return "registrationRequestConfirmationPage.jsp";
	}

	@Override
	public Model getModel() {
		return new BindingAwareModelMap();
	}

}
