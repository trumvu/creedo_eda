package de.unibonn.creedo.ui.indexpage;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ServerPaths;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.ui.PageBuilder;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.ValueValidator;

/**
 *
 * @author bjacobs
 * 
 */
public class DefaultIndexPageBuilder implements PageBuilder,
		ParameterContainer, RuntimeBuilder<Page, CreedoSession> {

	private final DefaultParameterContainer parameterContainer = new DefaultParameterContainer(
			"Index page parameters");

	private final Parameter<String> title;
	private final Parameter<String> referenceName;
	private final RangeEnumerableParameter<String> contentPageFileName;
	private final Parameter<String> loginInviteParameter;

	public DefaultIndexPageBuilder() {
		title = new DefaultParameter<>("Title",
				"The title of the index page, e.g., \"Home\"", String.class,
				"", StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.ALWAYS_VALID_VALIDATOR, "");

		referenceName = new DefaultParameter<>(
				"Reference name",
				"A short name for referencing the index page, e.g., from the navbar.",
				String.class, "", StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.ALWAYS_VALID_VALIDATOR, "");

		contentPageFileName = new IdentifierInRepositoryParameter<Path>(
				"Content file name",
				"The name of the html file containing the main content of the index page.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories
						.getIdIsFilenameWithExtensionPredicate(Repositories.HTML_FILE_EXTENSIONS));

		loginInviteParameter = new DefaultParameter<String>(
				"Login invitation",
				"An invitation text that is diplayed below the maincontent to users who are not logged into the application with their accoint (should contain a link to open the login page).",
				String.class,
				"<a href=\"login.htm\">Log in</a> to see demo analytics dashboards...",
				StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		parameterContainer.addParameter(title);
		parameterContainer.addParameter(referenceName);
		parameterContainer.addParameter(contentPageFileName);
		parameterContainer.addParameter(loginInviteParameter);
	}

	@Override
	public Page build(CreedoSession session) {
		return new DefaultIndexPage(session, title.getCurrentValue(),
				referenceName.getCurrentValue(),
				ServerPaths.getContentPageContent(contentPageFileName
						.getCurrentValue()),
				loginInviteParameter.getCurrentValue());
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return parameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return parameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return parameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		parameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		parameterContainer.unloadMapValuesToParameters(crossOutMap);
	}
}
