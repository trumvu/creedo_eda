package de.unibonn.creedo.ui.indexpage;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.webapp.User;
import de.unibonn.creedo.webapp.dashboard.mining.DataTableProvider;
import de.unibonn.creedo.webapp.dashboard.mining.MiningDashboardModelBuilder;
import de.unibonn.creedo.webapp.dashboard.mining.MiningDashboardModelBuilderImplementation;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.StringParser;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class DashboardLinkBuilder implements
		RuntimeBuilder<DashboardLink, User>, ParameterContainer {

	private final Parameter<String> titleParameter;

	private final Parameter<String> descriptionParameter;

	private final Parameter<String> imagePathParameter;

	private final Parameter<String> imageCreditsParameter;

	private final Parameter<String> analyticsDashboardParameter;

	private final Parameter<Integer> datatableParameter;

	private final DefaultParameterContainer defaultParameterContainer;

	public DashboardLinkBuilder() {
		titleParameter = new DefaultParameter<String>("Title",
				"The title of the demo link.", String.class, "",
				StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");
		descriptionParameter = new DefaultParameter<String>(
				"Description",
				"A short description that explains to users what the demo is about.",
				String.class, "", StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		imagePathParameter = new IdentifierInRepositoryParameter<Path>(
				"Image",
				"Image file for the demo link.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories
						.getIdIsFilenameWithExtensionPredicate(Repositories.IMAGE_FILE_EXTENSIONS));

		imageCreditsParameter = new DefaultParameter<String>("Image credits",
				"Can be used to display credit information for the image.",
				String.class, "", StringParser.TAKE_FIRST_STRING_PARSER,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");
		// analyticsDashboardParameter = new DefaultParameter<Integer>(
		// "Analytics dashboard",
		// "Integer id of analytics dashboard to be used for demo.",
		// Integer.class, 1, StringParser.INTEGER_PARSER,
		// new ValueValidator.LargerThanThresholdValidator<Integer>(0),
		// "positive id");
		analyticsDashboardParameter = new IdentifierInRepositoryParameter<MiningSystemBuilder>(
				"Analytics dashboard",
				"Integer id of analytics dashboard to be used for demo.",
				ApplicationRepositories.MINING_SYSTEM_REPOSITORY);
		datatableParameter = new DefaultParameter<Integer>("Datatable",
				"Integer id of datatable to be used for demo.", Integer.class,
				1, StringParser.INTEGER_PARSER,
				new ValueValidator.LargerThanThresholdValidator<Integer>(0),
				"positive id");
		defaultParameterContainer = new DefaultParameterContainer(
				"Demo specification");
		defaultParameterContainer.addAllParameters(Arrays.asList(
				(Parameter<?>) titleParameter, descriptionParameter,
				imagePathParameter, imageCreditsParameter,
				analyticsDashboardParameter, datatableParameter));
	}

	@Override
	public DashboardLink build(User context) {
		MiningSystemBuilder builder = ApplicationRepositories.MINING_SYSTEM_REPOSITORY
				.get(analyticsDashboardParameter.getCurrentValue())
				.getContent();

		DataTableProvider tableProvider = new DataTableProvider.DbBasedDataTableProvider(
				datatableParameter.getCurrentValue());

		MiningDashboardModelBuilder modelBuilder = new MiningDashboardModelBuilderImplementation(
				tableProvider, builder);

		return new DashboardLink(titleParameter.getCurrentValue(),
				descriptionParameter.getCurrentValue(),
				imagePathParameter.getCurrentValue(),
				imageCreditsParameter.getCurrentValue(), modelBuilder);
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return defaultParameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return defaultParameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return defaultParameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		defaultParameterContainer.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		defaultParameterContainer.unloadMapValuesToParameters(crossOutMap);
	}

}
