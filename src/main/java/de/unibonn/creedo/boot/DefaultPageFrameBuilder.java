package de.unibonn.creedo.boot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.common.DefaultSubCollectionParameter;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.PageBuilder;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.DefaultPageFrame;
import de.unibonn.creedo.ui.core.LoadPageAction;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.PageContainer;
import de.unibonn.creedo.ui.core.PageFrame;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

/**
 * <p>
 * Creates and configures a frame that can display pages.
 * </p>
 * 
 * <p>
 * WARNING: As of now this class is referenced directly in a db-based
 * repository. So make sure to update init scripts when moving or renaming this
 * class as well when you change its declared parameters.
 * </p>
 * 
 * @author mboley
 * 
 */
public class DefaultPageFrameBuilder implements
		RuntimeBuilder<PageFrame, CreedoSession>, ParameterContainer {

	private final DefaultParameterContainer defaultParameterContainer;

	private final SubCollectionParameter<String, List<String>> optionalNavbarContents;

	private final SubCollectionParameter<String, List<String>> optionalFooterContents;

	public DefaultPageFrameBuilder() {
		this.defaultParameterContainer = new DefaultParameterContainer(
				"Main frame parameters");
		DefaultSubCollectionParameter.CollectionComputer<List<String>> collectionComputer = new DefaultSubCollectionParameter.CollectionComputer<List<String>>() {

			@Override
			public List<String> computeCollection() {
				return ApplicationRepositories.PAGE_REPOSITORY.getAllIds();
			}
		};
		this.optionalNavbarContents = DefaultSubCollectionParameter
				.getDefaultSubListParameter(
						"Navbar entries",
						"List of optional content to be linked from the navbar of frame.",

						collectionComputer);
		this.optionalFooterContents = DefaultSubCollectionParameter
				.getDefaultSubListParameter(
						"Footer entries",
						"List of optional content to be linked from the footer of frame.",
						new DefaultSubCollectionParameter.CollectionComputer<List<String>>() {

							@Override
							public List<String> computeCollection() {
								return ApplicationRepositories.PAGE_REPOSITORY
										.getAllIds();
							}
						});
		this.defaultParameterContainer.addParameter(optionalNavbarContents);
		this.defaultParameterContainer.addParameter(optionalFooterContents);
	}

	@Override
	public PageFrame build(final CreedoSession creedoSession) {
		PageContainer pageContainer = new PageContainer(creedoSession
				.getUiRegister().getNextId());

		List<Action> navbarActions = new ArrayList<>();
		List<Action> footerActions = new ArrayList<>();

		int nextActionIdSuffix = 0;
		for (String contentId : optionalNavbarContents.getCurrentValue()) {
			RepositoryEntry<RuntimeBuilder<Page, CreedoSession>> entry = ApplicationRepositories.PAGE_REPOSITORY
					.get(contentId);
			// PageBuilder pageBuilder = pagesDAO.getPageBuilder(pageId);
			PageBuilder pageBuilder = (PageBuilder) entry.getContent();
			Page page = pageBuilder.build(creedoSession);

			navbarActions.add(new LoadPageAction(creedoSession.getUiRegister()
					.getNextId(), page, pageContainer));

		}
		for (String contentId : optionalFooterContents.getCurrentValue()) {
			RepositoryEntry<RuntimeBuilder<Page, CreedoSession>> entry = ApplicationRepositories.PAGE_REPOSITORY
					.get(contentId);
			PageBuilder pageBuilder = (PageBuilder) entry.getContent();
			Page page = pageBuilder.build(creedoSession);
			footerActions.add(new LoadPageAction(creedoSession.getUiRegister()
					.getNextId(), page, pageContainer));
		}

		navbarActions.add(new Action() {

			private final int id = creedoSession.getUiRegister().getNextId();

			@Override
			public String getReferenceName() {
				return "Close";
			}

			@Override
			public ResponseEntity<String> activate(String... params) {
				return new ResponseEntity<String>(HttpStatus.OK);
			}

			@Override
			public ClientWindowEffect getEffect() {
				return Action.ClientWindowEffect.CLOSE;
			}

			@Override
			public int getId() {
				return id;
			}
		});

		DefaultPageFrame result = creedoSession
				.getUiRegister()
				.createDefaultFrame(pageContainer, navbarActions, footerActions);

		// if ()
		// result.performAction(loadIndexPageActionId);

		return result;
	}

	@Override
	public Parameter<?> findParameterByName(String name) {
		return this.defaultParameterContainer.findParameterByName(name);
	}

	@Override
	public List<Parameter> getTopLevelParameters() {
		return this.defaultParameterContainer.getTopLevelParameters();
	}

	@Override
	public boolean isStateValid() {
		return this.defaultParameterContainer.isStateValid();
	}

	@Override
	public void passValuesToParameters(Map<String, String[]> nameValueMap) {
		this.passValuesToParameters(nameValueMap);
	}

	@Override
	public void unloadMapValuesToParameters(Map<String, String[]> crossOutMap) {
		this.unloadMapValuesToParameters(crossOutMap);
	}

}
