package de.unibonn.creedo.common;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

@Component
public class ConnectionFactory {

	public static ConnectionFactory get() {
		return (ConnectionFactory) ContextProvider.getApplicationContext().getBean("connectionFactory");
	}

    private SqlSessionFactory sqlSessionFactory;

	public ConnectionFactory(ConfigurationProperties config) {
			String connectionString = "jdbc:" + config.CREEDO_DB_SYSTEM + "://" + config.CREEDO_DB_HOST + "/" + config.CREEDO_DB_SCHEMA;

			Map<String, String> replacements = new HashMap<>();
			replacements.put("url", connectionString);
			replacements.put("driver", config.CREEDO_DB_DRIVER);
			replacements.put("user", config.CREEDO_DB_USER);
			replacements.put("pass", config.CREEDO_DB_PASS);

	    /* Since MyBatis configuration XML file only allows a static configuration and no dynamic elements,
		 * we have to modify this configuration before it is used in the builder. The modification consists
	     * of replacing the place-holders in the file with data read from the config file. (Which the user
	     * provided on start-up.
	     */
			try {
				Reader rawResource = Resources.getResourceAsReader("mybatis.config.xml");
				StringReader modifiedResource = new StringReader(replace(rawResource, replacements));

				sqlSessionFactory = new SqlSessionFactoryBuilder().build(modifiedResource);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	public SqlSessionFactory getSession() {
		return sqlSessionFactory;
    }

    /**
     * Reads line-wise from the input reader and replaces all occurrences of keys provided in the
     * Properties object with the associated values. The replacement is done on the prefix
     * "${ + key + }".
     * @param input Input data
     * @param replacements Replacements map
     * @return String containing the input data but with replacements applied
     */
    private static String replace(Reader input, Map<String, String> replacements) {
	BufferedReader reader = new BufferedReader(input);
	StringBuilder builder = new StringBuilder();

	String line;
	try {
	    while ((line = reader.readLine()) != null) {
		String newLine = line;
		for (Object object : replacements.keySet()) {
		    String key = (String) object;
		    newLine = newLine.replace("${" + key + "}", replacements.get(key));
		}
		builder.append(newLine).append("\n");
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}

	return builder.toString();
    }


}