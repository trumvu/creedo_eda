package de.unibonn.creedo.common;

/**
 * Created by IntelliJ IDEA. Date: 25/11/13
 */
public class Pair<K, V> {

	private final K lhs;

	private final V rhs;

	public Pair(K lhs, V rhs) {
		this.lhs = lhs;
		this.rhs = rhs;
	}

	public K getLhs() {
		return lhs;
	}

	public V getRhs() {
		return rhs;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Pair))
			return false;
		if (this == obj)
			return true;
		return equal(lhs, ((Pair) obj).getLhs())
				&& equal(rhs, ((Pair) obj).getRhs());
	}

	@Override
	public int hashCode() {
		return 31 * (lhs == null ? 0 : lhs.hashCode())
				+ (rhs == null ? 0 : rhs.hashCode());
	}

	private boolean equal(Object o1, Object o2) {
		return o1 == null ? o2 == null : (o1 == o2 || o1.equals(o2));
	}

	@Override
	public String toString() {
		return lhs + " preferred to " + rhs;
	}
}
