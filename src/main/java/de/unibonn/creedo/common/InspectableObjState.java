package de.unibonn.creedo.common;

import java.util.List;

/**
 * Provide the internal state of an inspectable with a table and a table header.
 * 
 * @author bkang
 *
 */

public interface InspectableObjState {

	public List<String> getHeader();

	public List<List<String>> getTable();
}
