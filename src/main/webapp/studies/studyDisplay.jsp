<jsp:include page="../includes/_header.jsp"/>
<jsp:include page="../static/navbar.jsp" />

<div class="col-xs-8 col-xs-offset-2">
    <div class="container">
	    <div class="col-xs-12">
	    <h2><small><span class="bold">Analysis task instructions</span></small></h2>
        </div>
        ${instructionHtml}

</div>

<jsp:include page="../includes/_footer.jsp"/>