<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${not empty statusAddDatabase}">
    <div
            class="alert alert-info alert-dismissible col-xs-8 col-xs-offset-2"
            role="alert">
        <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
        </button>
        <strong>Status:</strong> ${statusAddDatabase}
    </div>
</c:if>

<div class="col-xs-8 col-xs-offset-2">
    <div class="container">
        <form class="form-horizontal" role="form" id="uploadDatabaseForm" method="post"
              enctype="multipart/form-data"
              action="${pageContext.request.contextPath}/customDash/uploadDatabase.htm">

            <label for="databasename">Name:</label>
            <input type="text" class="form-control" id="databasename" name="databasename" form="uploadDatabaseForm"/>

            <label for="databasedescription">Description:</label>
            <input type="text" class="form-control" id="databasedescription" name="databasedescription"
                   form="uploadDatabaseForm"/>

            <label for="datafile">Data content file:</label>
            <input type="file" class="form-control file-upload" id="datafile" name="datafile"
                   form="uploadDatabaseForm"/>

            <label for="attributesfile">Attributes definition file:</label>
            <input type="file" class="form-control file-upload" id="attributesfile" name="attributesfile"
                   form="uploadDatabaseForm"/>

            <label for="attributegroupsfile">Attribute-groups definition file (optional):</label>
            <input type="file" class="form-control file-upload" id="attributegroupsfile" name="attributegroupsfile"
                   form="uploadDatabaseForm"/>

            <label for="datadelimiter">Data column delimiter (1 character):</label>
            <input type="text" class="form-control" id="datadelimiter" name="datadelimiter" form="uploadDatabaseForm"/>

            <label for="missingsymbol">Missing value indicator:</label>
            <input type="text" class="form-control" id="missingsymbol" name="missingsymbol" form="uploadDatabaseForm"/>

            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>