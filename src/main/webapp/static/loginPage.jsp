<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-xs-6 col-xs-offset-2">
		<form class="form-horizontal" role="form" method="POST" action="">
			<div class="form-group">
				<label for="username" class="col-xs-4 control-label">Email</label>
				<div class="col-xs-8">
					<input type="email" class="form-control" id="username"
						name="username" placeholder="Email">
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-xs-4 control-label">Password</label>
				<div class="col-xs-8">
					<input type="password" class="form-control" id="password"
						name="password" placeholder="Password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-offset-4 col-xs-3">
					<a href="javascript:{}"
						onclick="performAction(${loginPageComponentId}, '${loginAction.id}',[$('#username').val(), $('#password').val()], redirect); return false;"
						class="btn btn-primary">${loginAction.referenceName}</a>
				</div>
				<div class="col-xs-2 text-center" style="padding-top: 8px;">
					<strong>or</strong>
				</div>
				<div class="col-xs-3 text-right">
					<a href="#" class="actionLink btn btn-default"
						linkId="${loadAccountRequestPageAction.id}" resultTarget="REFRESH"
						frameId="${loginPageComponentId}">${loadAccountRequestPageAction.referenceName}</a>
				</div>
			</div>
		</form>
	</div>
	<div class="col-xs-3"></div>
</div>