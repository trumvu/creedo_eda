<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../includes/_header.jsp" />

<c:forEach items="${components}" var="component">
	<jsp:include page="${component.view}" />
</c:forEach>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="client/lib/bootstrap.min.js"></script>
<script src="client/lib/bootstrap-tour.min.js"></script>
<script src="client/lib/bootstrap-multiselect.js"></script>
<script src="client/lib/jquery.dataTables.min.js"></script>
<script src="client/lib/jquery.confirm.min.js"></script>
<script src="client/lib/dataTables.fixedColumns.min.js"></script>
<script src="client/lib/jquery-ui-1.10.4.custom.min.js"></script>
<!-- <script src="client/lib/jquery.form.js"></script> -->
<script src="client/lib/highcharts.js"></script>
<script src="client/lib/highChartsExporting.js"></script>
<script src="client/lib/bootbox.min.js"></script>

<!-- non-library js files -->
<script src="client/js/common.js?v=9"></script>
<jsp:include page="../dashboard/mineButton.jsp" />
<script src="client/js/learningSteps.js"></script>
<!-- default js code for the app -->
<script src="client/js/pointCloud.js?v=8"></script>
<script src="client/js/customDashboard.js?v=23"></script>
<script src="client/js/studyAdminDash.js"></script>
<script src="client/js/parameterHandler.js?v=11"></script>
<script src="client/js/default.js?v=68"></script>
<c:if test="${not empty ping and ping}">
	<script src="client/js/ping.js"></script>
</c:if>
</body>
</html>