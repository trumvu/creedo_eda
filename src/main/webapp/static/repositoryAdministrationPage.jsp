<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script src="client/js/parameterHandler.js"></script>

<div class="col-xs-8 col-xs-offset-2">

	<%-- Add button here --%>
	<div class="row paramMargin">
		<form class="form-vertical" role="form" method="POST" action="">
			<div class="form-group col-xs-1">
				<!-- 				<div class="col-xs-3 text-left"> -->
				<a href="javascript:{}"
					onclick="performAction(${componentId}, ${entryAdditionAction.id},[$('#identifier').val(),$('#class').val()]); return false;"
					class="btn btn-default">${entryAdditionAction.referenceName}</a>
				<!-- 				</div> -->
			</div>
			<div class="form-group col-xs-5 col-xs-offset-1">
				<!-- 				<div class="col-xs-10"> -->
				<input class="form-control" id="identifier" name="identifier"
					placeholder="Identifier" />
				<!-- 				</div> -->
			</div>
			<div class="form-group col-xs-5">
				<!-- 				<div class="col-xs-10"> -->
				<select class="form-control" id="class" name="class"
					placeholder="Class">
					<c:forEach items="${additionTypes}" var="type" varStatus="loop">
						<option value=${loop.index}>${type}</option>
					</c:forEach>
				</select>
				<!-- 				</div> -->
			</div>
		</form>
	</div>

	<div>
		<c:forEach items="${entries}" var="entry" varStatus="i">
			<div class="row paramMargin vertical-align">
				<div class="col-xs-11">
					<h3>${entry.id}(${entry.name})</h3>
				</div>
				<div class="col-xs-1">
					<a class="btn btn-default btn-block actionLink confirmAction"
						linkId="${entryDeletionAction.id}" resultTarget="REFRESH"
						frameId="${componentId}" actionParameter="${entry.id}">${entryDeletionAction.referenceName}</a>
				</div>
			</div>
			<div id="pageBuilderParameters_${i.index}"></div>
			<%-- Div id needed ? --%>
			<c:forEach items="${entry.parameters}" var="parameter" varStatus="j">
				<%--
            <div>
                <h3>Debug</h3>
                Range: <c:out value="${parameter.range}"/><br>
                Mult: <c:out value="${parameter.multiplicity}"/><br>
                Name: <c:out value="${parameter.name}"/><br>
                Descr.: <c:out value="${parameter.description}"/><br>
                SolHint: <c:out value="${parameter.solutionHint}"/><br>
                DepNotice: <c:out value="${parameter.dependsOnNotice}"/><br>
                Value: <c:out value="${parameter.values}"/><br>
                ActionID: <c:out value="${parameter.actionId}"/><br>
                Depth: <c:out value="${parameter.depth}"/><br>
                Valid: <c:out value="${parameter.valid}"/><br>
                Active: <c:out value="${parameter.active}"/><br>
            </div>
--%>
				<div class="row paramMargin">
					<div
						class="col-xs-<c:out value="${4 + parameter.depth}"/> text-right">
						<label for="${parameter.name}" data-toggle="tooltip"
							data-placement="top" title=" ${parameter.description}">${parameter.name}:&nbsp;</label>
						<c:if test="${not parameter.active}">
							<span
								class="glyphicon glyphicon-exclamation-sign red-tooltip red-icon"
								data-toggle="tooltip" data-placement="top"
								title=" ${current.dependsOnNotice}"></span>
						</c:if>
						<c:if test="${not parameter.valid}">
							<span
								class="glyphicon glyphicon-exclamation-sign red-tooltip red-icon"
								data-toggle="tooltip" data-placement="top"
								title=" ${current.solutionHint}"></span>
						</c:if>
					</div>
					<div class="col-xs-<c:out value="${8 - parameter.depth}"/>">
						<div
							class="input-group col-xs-12 <c:if test="${(not parameter.active) || (not parameter.valid)}">has-error</c:if>">
							<c:choose>
								<c:when test="${parameter.type == 'Text'}">
									<input type="text" class="form-control text-parameter"
										paramName="${parameter.name}"
										value="<c:out value="${parameter.values[0]}"/>"
										actionId="${parameter.actionId}" componentId="${componentId}" />
								</c:when>

								<c:when test="${parameter.type == 'RangeEnumerable'}">
									<select class="form-control range-enumerable-parameter"
										paramName="${parameter.name}" actionId="${parameter.actionId}"
										componentId="${componentId}"
										<c:if test="${not parameter.active}">disabled</c:if>>
										<c:if test="${parameter.active}">
											<option>Please select</option>
											<c:forEach items="${parameter.range}" var="option">
												<option value="${option}"
													<c:if test="${parameter.values[0] == option}">selected</c:if>><c:out
														value="${option}" /></option>
											</c:forEach>
										</c:if>
									</select>
								</c:when>

								<c:when test="${parameter.type == 'Subset'}">
									<select class="form-control subset-parameter multiple-select"
										paramName="${parameter.name}" actionId="${parameter.actionId}"
										componentId="${componentId}"
										<c:if test="${not parameter.active}">disabled</c:if> multiple>
										<c:if test="${parameter.active}">
											<c:forEach items="${parameter.range}" var="option">
												<c:set var="contains" value="false" />
												<c:forEach var="item" items="${parameter.values}">
													<c:if test="${item eq option}">
														<c:set var="contains" value="true" />
													</c:if>
												</c:forEach>
												<option value="${option}"
													<c:if test="${contains}">selected</c:if>><c:out
														value="${option}" /></option>
											</c:forEach>
										</c:if>
									</select>
								</c:when>

							</c:choose>
						</div>
					</div>
				</div>
			</c:forEach>
		</c:forEach>
	</div>
	<script language="JavaScript">
        $(document).ready(function () {
            // Register change handler
            $('.text-parameter, .range-enumerable-parameter').change(function() {
				registerParameterHasChanged($(this));
			});
            $('.subset-parameter').multiselect({
				buttonWidth: '100%',
				onChange: function(option) {
					var changedMultiSelect = $(option).parent();
					changedMultiSelect.addClass('modified-multiselect');
				},
                onDropdownHidden: function() {
					var changedMultiSelect = $('.modified-multiselect');
					if (changedMultiSelect.length) {
						changedMultiSelect.removeClass('modified-multiselect');
						registerParameterHasChanged(changedMultiSelect);
					}
				}
            });
        });
    </script>
</div>