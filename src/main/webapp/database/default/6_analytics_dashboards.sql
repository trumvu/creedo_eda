
INSERT INTO `mining__mining_systems` (`id`,`content_class_name`) VALUES ('creedo_default_system','de.unibonn.creedo.webapp.dashboard.mining.ManualMiningSystemBuilder');
INSERT INTO `mining__mining_systems` (`id`,`content_class_name`) VALUES ('ass_sampling_forsied_postproc','de.unibonn.creedo.webapp.dashboard.mining.ManualMiningSystemBuilder');
INSERT INTO `mining__mining_systems` (`id`,`content_class_name`) VALUES ('ass_sampling_default_postproc','de.unibonn.creedo.webapp.dashboard.mining.ManualMiningSystemBuilder');

INSERT INTO `mining__mining_systems_parameters` (`id`,`entry_id`,`param_name`,`param_value`) VALUES (1,'creedo_default_system','Algorithms','[ASSOCIATION_BEAMSEARCH, EXCEPTIONAL_MODEL_BEAMSEARCH, ASSOCIATION_SAMPLER, EMM_SAMPLER]');
INSERT INTO `mining__mining_systems_parameters` (`id`,`entry_id`,`param_name`,`param_value`) VALUES (2,'creedo_default_system','Post processor','Default ranker');
INSERT INTO `mining__mining_systems_parameters` (`id`,`entry_id`,`param_name`,`param_value`) VALUES (3,'ass_sampling_forsied_postproc','Algorithms','[ASSOCIATION_SAMPLER]');
INSERT INTO `mining__mining_systems_parameters` (`id`,`entry_id`,`param_name`,`param_value`) VALUES (4,'ass_sampling_forsied_postproc','Post processor','Knowledge model ranker');
INSERT INTO `mining__mining_systems_parameters` (`id`,`entry_id`,`param_name`,`param_value`) VALUES (5,'ass_sampling_default_postproc','Algorithms','[ASSOCIATION_SAMPLER]');
INSERT INTO `mining__mining_systems_parameters` (`id`,`entry_id`,`param_name`,`param_value`) VALUES (6,'ass_sampling_default_postproc','Post processor','Default ranker');
