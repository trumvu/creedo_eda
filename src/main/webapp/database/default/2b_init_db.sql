/**
 * Create Study related schemes.
 */

/*
 * 0. Table study__evaluationSchemes
 */
CREATE TABLE `study__evaluation_schemes` (
  `id` 			INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `metrics` 	TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
 * 1. Table study__tasks: id, name, description, datatable_id, [evaluation scheme]
 */
CREATE TABLE `study__tasks` (
  `id` 			INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` 		TEXT,
  `description` TEXT,
  `datatable_id` INT,
  `evaluation_scheme_id` INT,
  FOREIGN KEY (`datatable_id`) REFERENCES `datatables` (`id`),
  FOREIGN KEY (`evaluation_scheme_id`) REFERENCES `study__evaluation_schemes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
 *2. Table study__task_instructions (copying old analysisTask_instructions table only referring to new table study__tasks as first foreign)
 */
CREATE TABLE `study__task_instructions` (
  `task_id` 	INT,
  `instruction_page_index` INT,
  `instruction_content` TEXT,
  FOREIGN KEY (`task_id`) REFERENCES `study__tasks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
 *8. Table study__sessions: id, study_id, user_id, task_id, system_id, .. (time)
 */
CREATE TABLE `study__sessions` (
  `id`    		INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `study_name` 	TEXT,
  `user_id` 	INT,
  `task_name` 	TEXT,
  `system_name` TEXT,
  `end_time`	DATETIME,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*
 *9. copies of results and evaluation tables (with study___ prefix)
 */
CREATE TABLE `study__task_session_results` (
  `id`              		INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `session_id`             	INT,
  `result_builder_content` 	TEXT,
  `seconds_in_session_until_saved` INT,
  FOREIGN KEY (`session_id`) REFERENCES `study__sessions` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
  
CREATE TABLE `study__result_ratings` (
  `id` 			INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `study_name` 	TEXT,
  `user_id` 	INT,
  `result_id` 	INT,
  `metric` 		TEXT,
  `value` 		DOUBLE,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  FOREIGN KEY (`result_id`) REFERENCES `study__task_session_results` (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE `study__evaluation_instructions` (
  `evaluation_scheme_id` 	INT,
  `instruction_page_index` INT,
  `instruction_content` TEXT,
  FOREIGN KEY (`evaluation_scheme_id`) REFERENCES `study__evaluation_schemes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
 * study design repository
 */
CREATE TABLE `content__study_designs`
(
  `id`                  VARCHAR(255) PRIMARY KEY NOT NULL,
  `content_class_name`  TEXT
);

CREATE TABLE `content__study_designs_parameters` (
  `id`          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `entry_id`  VARCHAR(255),
  `param_name`  TEXT,
  `param_value` TEXT,
  FOREIGN KEY (`entry_id`) REFERENCES `content__study_designs` (`id`)
);

/*
 * study repository
 */
CREATE TABLE `content__studies`
(
  `id`                  VARCHAR(255) PRIMARY KEY NOT NULL,
  `content_class_name`  TEXT
);

CREATE TABLE `content__studies_parameters` (
  `id`          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `entry_id`  VARCHAR(255),
  `param_name`  TEXT,
  `param_value` TEXT,
  FOREIGN KEY (`entry_id`) REFERENCES `content__studies` (`id`)
);

/*
 * study system specification repository
 */
CREATE TABLE `content__study_system_specifications`
(
  `id`                  VARCHAR(255) PRIMARY KEY NOT NULL,
  `content_class_name`  TEXT
);

CREATE TABLE `content__study_system_specifications_parameters` (
  `id`          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `entry_id`  VARCHAR(255),
  `param_name`  TEXT,
  `param_value` TEXT,
  FOREIGN KEY (`entry_id`) REFERENCES `content__study_system_specifications` (`id`)
);

/*
 * study task specification repository
 */
CREATE TABLE `content__study_task_specifications`
(
  `id`                  VARCHAR(255) PRIMARY KEY NOT NULL,
  `content_class_name`  TEXT
);

CREATE TABLE `content__study_task_specifications_parameters` (
  `id`          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `entry_id`  VARCHAR(255),
  `param_name`  TEXT,
  `param_value` TEXT,
  FOREIGN KEY (`entry_id`) REFERENCES `content__study_task_specifications` (`id`)
);

/*
 * study evaluation scheme repository
 */
CREATE TABLE `content__study_evaluation_schemes`
(
  `id`                  VARCHAR(255) PRIMARY KEY NOT NULL,
  `content_class_name`  TEXT
);

CREATE TABLE `content__study_evaluation_schemes_parameters` (
  `id`          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `entry_id`  VARCHAR(255),
  `param_name`  TEXT,
  `param_value` TEXT,
  FOREIGN KEY (`entry_id`) REFERENCES `content__study_evaluation_schemes` (`id`)
);