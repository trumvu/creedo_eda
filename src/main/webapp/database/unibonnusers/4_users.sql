
# Delete current users
DELETE FROM `user`;

# Insert users
INSERT INTO `user` VALUES (1, 'bjoern.jacobs@iais.fraunhofer.de', '$2a$12$Af4s3sjC1sx6uDJ4MR9pEu.QAImohGmYUektCsawgDPRhi9KqKNd2', 1, 1, 1, 1, '2014-05-01 10:00:00');
INSERT INTO `user` VALUES (2, 'test@test.com', '$2a$12$xaR9ZG7V45zwO5IROjvxC./FVFUXqAQc9m3TfqWfzlUy.ZffgMptG', 1, 1, 0, 0, '2014-05-01 11:00:00');
INSERT INTO `user` VALUES (3, 'ref@test.com', '$2a$12$xaR9ZG7V45zwO5IROjvxC./FVFUXqAQc9m3TfqWfzlUy.ZffgMptG', 1, 1, 0, 0, '2014-05-01 11:00:01');
INSERT INTO `user` VALUES (4, 'mario.boley@iais.fraunhofer.de', '$2a$12$PWuX9hkr7PQEf0hNkPHXoeKq6DAn8TJHUoQ5P7YAEKp.axZPYZDi.', 1, 1, 1, 0, '2014-05-01 11:00:01');
INSERT INTO `user` VALUES (5, 'kd-tester@iais.fraunhofer.de', '$2a$12$fuS6GFwTRMsMxKi7.De09.M2PRshM5TPmKOJYn.rRy1lE.M7HONFi', 0, 1, 0, 0, '2014-05-01 11:00:01');
INSERT INTO `user` VALUES (6, 'michael.kamp@iais.fraunhofer.de', '$2a$12$MizD8TlVv5YGhXycYxRqoe70hZyDsRRwfWPdq6v9jXxa7BVORiv5K', 1, 1, 0, 0, '2014-10-14 12:34:17.0');
INSERT INTO `user` VALUES (7, 'bo.kang@uni-bonn.de', '$2a$12$HmXxFRjC2rrB2A6jG0DgguF2nTu3mrNCvjLEckdzjPKYoX.qhAp36', 1, 1, 1, 0, '2015-01-22 12:34:17.0');

