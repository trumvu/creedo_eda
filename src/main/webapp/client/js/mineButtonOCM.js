function ocmMine(e) {
    var $that = $(this);
    var $container = $("#candidate-patterns");
    $("#analysis-patterns").addClass("disabled");
    $container.html("<div class=\"text-center\"><img src=\"client/images/loader.gif\" /></div>");
    e.preventDefault();
    $that.addClass('disabled');

    $('#mining-error-notification').addClass('hidden');

    $.ajax({
        url: 'mine.htm',
        type: 'post',
        beforeSend: function (request)
        {
            var aid = window.analysisSessionId;
            request.setRequestHeader("analysisSessionId", aid);
        },
        error: function (request, status, error) {
            showMiningError(request.responseText);
            $container.html("");
        },
        success: function (data) {
            $container.html(data);
            // activate tooltip on patterns
            $container.find("[data-toggle=tooltip]").tooltip();

            // make patterns draggable
            $container.find(".pattern").draggable({
                revert: 'invalid',
                appendTo: "body",
                helper: function (event, ui) {
                    var helper = $(this).clone();
                    helper.css('width', $(this).css('width'));
                    helper.css('height', $(this).css('height'));
                    return helper;
                },
                start: function (evt, ui) {
                    $(this).css("display", "none");
                },
                revert: function (valid) {
                    if (!valid) {
                        $(this).css("display", "block");
                        return true;
                    } else {
                        return false;
                    }
                }
            });
        },
        complete: function () {
            $that.removeClass('disabled');
            $("#analysis-patterns").removeClass("disabled");
        }});
}

$(document).ready(function () {
    $("#btn-start-mining").on("click", ocmMine);//.click();
});
