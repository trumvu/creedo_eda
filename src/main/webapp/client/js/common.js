function getNewPopUpWindow(url) {
	var result = window.open(url, "_blank",
			"scrollbars=1,toolbar=0,location=0,menubar=0,status=0,height="
					+ screen.height + ",width=" + screen.width);
	// result.displayDashboardFromUrl = function(dashboardUrl) {
	// this.location.replace(dashboardUrl);
	// };
	return result;
}

function showMessage(text) {
	bootbox.dialog({
		message : text,
		buttons : {
			success : {
				label : "OK",
				className : "btn-success"
			}
		}
	});
}

function loadFrameInNewWindow(postUrl, postData, target) {
	$.ajax({
		type : "POST",
		url : postUrl,
		data : postData,
		dataType : "text",
		timeout : 30000, // in milliseconds
		success : function(url) {
			target.location.replace(url);
			// target.displayDashboardFromUrl(url);
			target.focus();
		},
		error : function(request, status, err) {
			target.close();
			if (status == "timeout") {
				alert(status);
			}
		}
	});
}

function refresh() {
	var currentLocation = window.location.href;
	window.location.href = currentLocation;
}

function redirect(url) {
	window.location.href = url;
}

function defaultErrorHandler(xhr) {
	showMessage(xhr.responseText);
}

function performAction(actionProviderId, actionId, parameters, successHandler,
		errorHandler) {
	successHandler = (typeof successHandler !== 'undefined' ? successHandler
			: refresh);
	errorHandler = (typeof errorHandler !== 'undefined' ? errorHandler
			: defaultErrorHandler);
	$.post("performAction.htm", {
		frameId : actionProviderId,
		linkId : actionId,
		parameter : parameters,
	}).success(successHandler).error(errorHandler);
}

function performActionAndAwaitResultInNewPopUp(frameId, linkId) {
	var popUp = getNewPopUpWindow("loading.htm");
	performAction(frameId, linkId, [], function(url) {
		popUp.location.replace(url);
		popUp.focus();
	}, function(xhr) {
		showMessage(xhr.responseText);
	});
}
