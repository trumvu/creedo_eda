var categoriesSelect = $('#algorithmCategoriesSelect');
var algorithmsArea = $('#refAlgorithmsArea');
var algorithmsSelect = $('#algorithmSelect');
var parametersArea = $('#refParametersArea');
var executeButton = $('#refExecuteButton');
var candidatesContainer = $("#candidate-patterns");
var resultsPatterns = $('#analysis-patterns');
var refBox = $('#refControlBox');
var mineButton = $('#btn-start-mining');
var stopButton = $('#btn-stop-mining');
var isCurrentAlgorithmStoppable = true;


function refMine(e) {
    e.preventDefault();

    /* Reference mining system logic */
    refBox.modal();
}

function getCategories() {
    categoriesSelect.html('<option value="">Select algorithm category</option>');

    $.post('/Creedo/manualmining/getcategories.json', { "dashboardId" : window.analysisSessionId }, function(results) {
        $.each(results, function (index, current) {
            categoriesSelect.append($("<option />").val(current.category).text(current.description));
        });
    });
}

function getCurrentSelectedCategory() {
    return categoriesSelect.find(":selected").val();
}

function getCategoryAlgorithmsEventHandler() {
    // When category is changed, hide all parameters selected so far
    parametersArea.hide();

    var selected = getCurrentSelectedCategory();
    if (selected == "") {
        algorithmsArea.hide();
        executeButton.addClass('disabled');
    } else {
        getAndShowCategoryAlgorithms(selected);
    }
}

/* Algorithm selection */
function getAndShowCategoryAlgorithms(category) {
    $.post("/Creedo/manualmining/getcategoryalgorithms.json", {"algorithmCategory": category, "dashboardId" : window.analysisSessionId }, function (results) {

        algorithmsSelect.html($("<option />").val("").text("Select algorithm"));
        $.each(results, function (index, current) {
            algorithmsSelect.append($("<option />").val(current.name).text(current.name));
        });

        algorithmsArea.show();
    });
}

function getCurrentSelectedAlgorithm() {
    return algorithmsSelect.find(":selected").val();
}

/* Parameter selection */
function getAlgorithmParametersEventHandler() {
    var selected = getCurrentSelectedAlgorithm();
    if (selected == "") {
        parametersArea.hide();
        executeButton.addClass('disabled');
    } else {
        getAndShowAlgorithmParameters(selected);
    }
}

function getAndShowAlgorithmParameters(algorithmName) {
    $.post("/Creedo/manualmining/getalgorithmparameters.json", {"algorithmName": algorithmName, "dashboardId" : window.analysisSessionId}, updateParameterArea);
}

/**
 * Maps the received parameters to visible control elements. Furthermore it checks for verifiability and validity of
 * the values and conditionally shows hint icons and dis/en-ables the execute button.
 * @param results
 */
function updateParameterArea(results) {
    parametersArea.empty();
    parametersArea.hide();

    var allOk = true;
    executeButton.addClass('disabled');

    var allOk = renderParameters(parametersArea, results);

/*    // Iterate over received parameter configurations
    $.each(results, function (index, current) {
        // Build columns
        var paramColLeft = $('<div/>', {class: 'col-xs-4 text-right'});
        var paramColRight = $('<div/>', {class: 'col-xs-8'});

        // Create a new row
        var row = $('<div/>', {class: 'row paramMargin'});
        row.append(paramColLeft);
        row.append(paramColRight);

        // Create label and attach it to left column
        var newLabel = $('<label/>', {for: current.paramIdentifier});
        newLabel.html(current.name + ":&nbsp;");

        $(newLabel).attr({
            'data-toggle': 'tooltip',
            'data-placement': 'top',
            title: current.description
        });

        paramColLeft.append(newLabel);

        // Create input div on the right column
        var newDiv = $('<div/>', {class: 'input-group col-xs-12'});

        // Generate solution hint (only use it when parameter active but not valid)
        var invalidValueSign = $('<span/>', {class: 'glyphicon glyphicon-exclamation-sign red-tooltip red-icon'});
        invalidValueSign.attr({
            'data-toggle': 'tooltip',
            'data-placement': 'bottom',
            title: current.solutionHint
        });

        // Generate not-verifiable-sign sign (only use it when parameter not active because of depending parameters)
        var notVerifiableSign = $('<span/>', {class: 'glyphicon glyphicon-exclamation-sign red-tooltip red-icon'});
        notVerifiableSign.attr({
            'data-toggle': 'tooltip',
            'data-placement': 'bottom',
            title: current.dependsOnNotice
        });

        isCurrentAlgorithmStoppable = current.stoppable;

        // Now differentiate between extensional and intentional
        if (current.multiplicity != null) {
            var newSelect = $('<select/>', {class: 'form-control extParameter ', id: current.paramIdentifier});

            // Extract selected value(s)
            var selectedValues = Object.create(null);
            if (current.multiplicity == 'multiple') {
                newSelect.prop('multiple', "multiple");
                newSelect.addClass('multiple-select');
                if (current.value != null) {
                    var tmpSelections = arrayStringToSet(current.value);
                    $.each(tmpSelections, function (index, current) {
                        selectedValues[current] = true;
                    })
                }
            } else {
                selectedValues[current.value] = true;
            }

            newDiv.append(newSelect);
            paramColRight.append(newDiv);

            if (!current.active) {
                newSelect.prop('disabled', true);
                newDiv.addClass('has-error');
                $(paramColLeft).append(notVerifiableSign);
                allOk = false;
            } else {
                $.each(current.range, function (index, current) {
                    var newOption = $('<option/>', { text: current, value: current });
                    if (current in selectedValues) {
                        newOption.prop('selected', true);
                    }
                    newSelect.append(newOption);
                });
                if (!current.valid) {
                    newDiv.addClass('has-error');
                    $(paramColLeft).append(invalidValueSign);
                    allOk = false;
                }
            }
        } else {
            var newInput = $('<input/>', {type: 'text', class: 'form-control intParameter', id: current.paramIdentifier, val: current.value});
            newInput.html(current.name);
            newDiv.append(newInput);
            paramColRight.append(newDiv);

            if (!current.active) {
                newInput.prop('disabled', true);
                newDiv.addClass('has-error');
                $(paramColLeft).append(notVerifiableSign);
                allOk = false;
            } else {
                if (!current.valid) {
                    newDiv.addClass('has-error');
                    $(paramColLeft).append(invalidValueSign);
                    allOk = false;
                }
            }
        }

        parametersArea.append(row);
    });
    */

    // Initialize tooltips
    $("[data-toggle=tooltip]").tooltip();
    $("[data-toggle=popover]").popover();

    // Initialize change event handler, but treat multiple-select differently!
    // $('.extParameter, .intParameter').not('.multiple-select').change(getCheckParametersHandler);
    
    $('.text-parameter, .range-enumerable-parameter').change(getCheckParametersHandler);
    $('.subset-parameter').multiselect({
        buttonWidth: '100%',
        onDropdownHidden: getCheckParametersHandler
    });

    parametersArea.show();

    if (allOk) {
        executeButton.removeClass('disabled');
    }
}

function getCheckParametersHandler() {
    var algorithmName = getCurrentSelectedAlgorithm();

    if (algorithmName != '') {
        var stringified = prepareParameterJsonPackage(algorithmName);
        $.post('/Creedo/manualmining/applyandgetalgorithmparameters.json', stringified, updateParameterArea);
    }
}

function getExecuteButtonEventHandler(e) {
    candidatesContainer.html("<div class=\"text-center\"><img src=\"client/images/loader.gif\" /></div>");
    e.preventDefault();

    // Hide box
    refBox.modal('hide');

    // Reset stop-button
    if (isCurrentAlgorithmStoppable) {
        stopButton.removeClass('disabled');
    } else {
        stopButton.addClass('disabled');
    }

    var algorithmName = getCurrentSelectedAlgorithm();

    if (algorithmName != '') {
        // Disable patterns areas
        candidatesContainer.addClass('disabled');
        resultsPatterns.addClass('disabled');

        // Hide "Mine" button, therefore show "Stop" button
        mineButton.addClass('hidden');
        stopButton.removeClass('hidden');

        $('#mining-error-notification').addClass('hidden');

        var stringified = prepareParameterJsonPackage(algorithmName);

        $.ajax({
            url: 'mine.htm',
            type: 'post',
            data: stringified,
            beforeSend: function (request)
            {
                var aid = window.analysisSessionId;
                request.setRequestHeader("analysisSessionId", aid);
            },
            error: function (request, status, error) {
                showMiningError(request.responseText);
                candidatesContainer.html("");
            },
            success: function (data, status, request) {
                candidatesContainer.html(data);
                // activate tooltip on patterns
                candidatesContainer.find("[data-toggle=tooltip]").tooltip();

                // make patterns draggable
                candidatesContainer.find(".pattern").draggable({
                    revert: 'invalid',
                    appendTo: "body",
                    helper: function (event, ui) {
                        var helper = $(this).clone();
                        helper.css('width', $(this).css('width'));
                        helper.css('height', $(this).css('height'));
                        return helper;
                    },
                    start: function (evt, ui) {
                        $(this).css("display", "none");
                    },
                    revert: function (valid) {
                        if (!valid) {
                            $(this).css("display", "block");
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
            },
            complete: function() {
                candidatesContainer.removeClass('disabled');
                resultsPatterns.removeClass('disabled');

                // Hide "Stop" button, therefore show "Mine" button
                stopButton.addClass('hidden');
                mineButton.removeClass('hidden');
            }
        });
    }
}

/* Stop-Button functionality in manual mining system */
function stopButtonClickEventListener(e) {
    e.preventDefault();

    $.post("/Creedo/manualmining/stopcurrentmining.json", { "dashboardId" : window.analysisSessionId }).always(function () {
        stopButton.addClass('disabled');
    });
}

function prepareParameterJsonPackage(algorithmName) {
    var params = $('.text-parameter, .range-enumerable-parameter, .subset-parameter');
    var filtered = params.filter(function () {
        return $(this).prop('disabled') === false;
    });

    // Build a data-object containing all parameter value selections by the user
    var data = $();
    data.algorithmName = algorithmName;
    data.dashboardId = window.analysisSessionId;
    var parameters = [];
    $.each(filtered, function (index, current) {
        var id = current.id;
        if (current.type == 'select-multiple') {
            // Only select the actual values of selected options
            var values = $.map(current.selectedOptions, function (option) {
                return option.value;
            });

            parameters.push({"id": id, "value": values})
        } else {
            parameters.push({"id": id, "value": [current.value]});
        }
    });

    data.dashboardId = window.analysisSessionId;
    data.parameters = parameters;
    return JSON.stringify(data);
}

$(document).ready(function () {
    executeButton.addClass('disabled');

    $("#btn-start-mining").click(refMine);
    getCategories();

    // Register event handler for change-events
    categoriesSelect.change(getCategoryAlgorithmsEventHandler);
    algorithmsSelect.change(getAlgorithmParametersEventHandler);

    // Register callback for execute button
    executeButton.click(getExecuteButtonEventHandler);
    stopButton.click(stopButtonClickEventListener);
});

